-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema gestion
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema gestion
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gestion` DEFAULT CHARACTER SET utf8 ;
USE `gestion` ;

-- -----------------------------------------------------
-- Table `gestion`.`clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestion`.`clientes` (
  `idclientes` INT NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `direccion` VARCHAR(45) NULL,
  `poblacion` VARCHAR(45) NULL,
  `telefono` VARCHAR(9) NULL,
  PRIMARY KEY (`idclientes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gestion`.`productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestion`.`productos` (
  `idproductos` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `precio` DECIMAL NOT NULL,
  PRIMARY KEY (`idproductos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gestion`.`facturas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestion`.`facturas` (
  `idfacturas` VARCHAR(9) NOT NULL,
  `fecha` DATE NOT NULL,
  `importetotal` DECIMAL(9,2) NULL,
  `clientes_idclientes` INT NOT NULL,
  PRIMARY KEY (`idfacturas`),
  CONSTRAINT `fk_facturas_clientes`
    FOREIGN KEY (`clientes_idclientes`)
    REFERENCES `gestion`.`clientes` (`idclientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `gestion`.`productos_has_facturas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestion`.`productos_has_facturas` (
  `productos_idproductos` INT NOT NULL,
  `facturas_idfacturas` VARCHAR(9) NOT NULL,
  `cantidad` INT NOT NULL,
  PRIMARY KEY (`productos_idproductos`, `facturas_idfacturas`),
  CONSTRAINT `fk_productos_has_facturas_productos1`
    FOREIGN KEY (`productos_idproductos`)
    REFERENCES `gestion`.`productos` (`idproductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_facturas_facturas1`
    FOREIGN KEY (`facturas_idfacturas`)
    REFERENCES `gestion`.`facturas` (`idfacturas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
