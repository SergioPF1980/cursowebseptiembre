<?php
    include_once 'includes/classes/conexion.php';
    include_once 'includes/classes/contact.php';
?>

    <p>Contact</p>

<?php
    $items = [];
    $items = Contact::getContacts();

    foreach ($items as $item): ?>
        <li>
            
            <p>
                Name: <?php echo $item->getName();?><br />
                Phone: <?php echo $item->getPhone();?><br />
                Address: <?php echo $item->getAddress();?><br />
            </p>
        </li>
    <?php endforeach;?>

<html>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<body>

    <div class="w3-container">
        <button onclick="document.getElementById('id01').style.display='block'"
            class="w3-button w3-green w3-large">Registro</button>

        <div id="id01" class="w3-modal">
            <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

                <div class="w3-center"><br>
                    <span onclick="document.getElementById('id01').style.display='none'"
                        class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
                    <h5>Register Contact</h5>
                </div>

                <form class="w3-container" action="../includes/register.php" method="POST">
                    <div class="w3-section">
                        <label><b>Name</b></label>
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Name"
                            name="name" required>
                        <label><b>Phone</b></label>
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Phone"
                            name="phone" required>
                        <label><b>Address</b></label>
                        <input class="w3-input w3-border" type="text" placeholder="Enter Address" name="address"
                            required>
                        
                        <input class="w3-input w3-border" type="hidden"  name="id">
                        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Registrar</button>
                    
                    </div>
                </form>

                <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                    <button onclick="document.getElementById('id01').style.display='none'" type="button"
                        class="w3-button w3-red">Cancel</button>
                </div>

            </div>
        </div>
    </div>

</body>

</html>