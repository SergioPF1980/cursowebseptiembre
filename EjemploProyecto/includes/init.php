<?php
    session_start();

    require_once 'includes/functions.php';

    spl_autoload_register(function ($class) {
        require_once 'classes/' . strtolower($class) . '.php';
    });
?>