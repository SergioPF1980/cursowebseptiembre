<?php

    class Contact{
        private $id;
        private $name;
        private $phone;
        private $address;


        public function __construct($entrada = false){
            if(is_array($entrada)){
                foreach($entrada as $clave=>$valor){
                    $this->$clave=$valor;
                }
            }
        }

        public function getID(){
            return $this->id;
        }

        public function getName(){
            return $this->name;
        }

        public function getPhone(){
            return $this->phone;
        }

        public function getAddress(){
            return $this->address;
        }

        public static function getContacts(){
            $items=[];
            $connection = Conexion::getConection();

            $query = "SELECT * FROM contacts";

            if($result = $connection->query($query)){
                while ($obj = $result->fetch_object('Contact')) {
                    $items[] = $obj;
                }
                $result->close();
            }else{
                return false;
            }

            return $items;
        }

        public function addRecord() {
            
            if ($this->_verifyInput()) {
                $connection = Conexion::getConection();
                
                $query = "INSERT INTO contacts(name, phone, address)
                VALUES ('" . Conexion::prep($this->name) . "', 
                        '" . Conexion::prep($this->phone) . "',
                        '" . Conexion::prep($this->address) . "')";
                
                if ($connection->query($query)) {
                    $return = array('Contacto añadido');
                    return $return;
                }
                else {
                    
                    $return = array('No se ha podido añadir a la BD.');
                    return $return;
                }
            }
            else {
                
                $return = array('No se ha añadido el contacto.');
                return $return;
            }
        }

        protected function _verifyInput() {
            $error = false;
                if (!trim($this->name)) {
                    $error = true;
                }
                if (!trim($this->phone)) {
                    $error = true;
                }
                if (!trim($this->address)) {
                    $error = true;
                }
                return !$error;
        }
    }
?>