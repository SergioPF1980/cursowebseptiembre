<?php
    session_start();

    require_once 'function.php';

    spl_autoload_register(function ($class) {
        require_once 'classes/' . strtolower($class) . '.php';
    });
?>