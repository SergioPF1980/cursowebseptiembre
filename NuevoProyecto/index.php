<?php

require_once 'includes/function.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Hola Mundo!!!!</h1>

    <?php 
        echo 'Hola Mundo de mi PHP';
        //Primer ejercicio
        // loadContent();
    ?>


    <ul>
        <!-- <li><a href="index.php?content=archivo">Archivo1</a></li>
        <li><a href="index.php?content=archivo2">Archivo2</a></li>
        <li><a href="index.php?content=archivo3">Archivo3</a></li>
        <li><a href="index.php?content=archivo4">Archivo4</a></li> -->
            <?php
                // loadContent(); Primera solucion
                loadContent(
                    isset($_GET['content']) ?? 'archivo',
                    isset($_GET['nav']) ?? null,
                );
            ?>

    </ul>

</body>
</html>