<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arrays</title>
</head>
<body>
    <h1>Arrays</h1>

    <!-- Array -->
    <?php
        $coche1 = Array("Renault","Megane",Array("5 puertas","Diesel","Rojo"));
        $coche2 = Array("Opel","Corsa",Array("3 puertas","Gasolina","Gris"));
        $coche3 = Array("Seat","Ibiza",Array("5 puertas","Diesel","Verde"));
        $coche4 = Array("Audi","TT",Array("3 puertas","Gasolina","Plateado"));
        $coches = Array($coche1,$coche2,$coche3,$coche4);
    ?>

    <!-- Imprimir Resultados -->
    <h3>Resultados con var_dum($coches)</h3>
    <?php
        var_dump($coches);
        /* Resultado:
        array(4) { [0]=> array(3) { [0]=> string(7) "Renault" [1]=> string(6) "Megane" [2]=> array(3) {
        [0]=> string(9) "5 puertas" [1]=> string(6) "Diesel" [2]=> string(4) "Rojo" } } [1]=> array(3) {
        [0]=> string(4) "Opel" [1]=> string(5) "Corsa" [2]=> array(3) { [0]=> string(9) "3 puertas"
        [1]=> string(8) "Gasolina" [2]=> string(4) "Gris" } } [2]=> array(3) { [0]=> string(4) "Seat"
        [1]=> string(5) "Ibiza" [2]=> array(3) { [0]=> string(9) "5 puertas" [1]=> string(6) "Diesel"
        [2]=> string(5) "Verde" } } [3]=> array(3) { [0]=> string(4) "Audi" [1]=> string(2) "TT" [2]=>
        array(3) { [0]=> string(9) "3 puertas" [1]=> string(8) "Gasolina" [2]=> string(8) "Plateado" } } }
        */
    ?>

    <!-- Imprimir Resultados Ordenados con la etiqueta pre-->
    <h3>Resultados con var_dum($coches). Ordenados con "pre"</h3>
    <?php
        echo '<pre>';
            var_dump($coches);
        echo '</pre>';
    ?>
    <h3>Resultados con print_r($coches). Ordenados con "pre"</h3>
    <?php
        echo '<pre>';
            print_r($coches);
        echo '</pre>';
    ?>
    <h3>Resultados de un elemento de la array</h3>
    <?php
        
        print_r($coches[1]);
        
    ?>

    <h3>Resultados array foreach</h3>
    <?php
        foreach($coches as $coche){
            echo '<pre>';
                print_r($coche) . "\n";
            echo '</pre>';
        }
    ?>
</body>
</html>