<?php

class Employee
{
    protected $id;
    protected $name;
    protected $email;
    protected $role;

    public function __construct($entrada = false)
    {
        if (is_array($entrada)) {
            foreach ($entrada as $clave => $valor) {
                $this->$clave = $valor;
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function addEmployee()
    {
        $connection = Database::getConnection();

        $query = "INSERT INTO employees(name, email, role)
						VALUES ('" . $this->name . "', '" . $this->email . "',
						'" . $this->role . "')";

        if ($connection->query($query)) {
            $return = array('', 'Usuario añadido correctamente');
            return $return;
        } else {
            $return = 'No se ha podido añadir el usuario a la BD.';
            return $return;
        }
    }


    public static function getEmployees()
    {
        $connection = Database::getConnection();
        $query = "SELECT * FROM employees";
        if (!$result = $connection->query($query)) {
            throw new Exception('Error en la consulta: ' . $connection->error);
        } else {
            $items = [];
            while ($fetchResult = $result->fetch_object('Employee')) {
                $items[] = $fetchResult;
            }
            return $items;
        }
    }

    public static function checkEmployeeEmail($email)
    {
        $conn = Database::getConnection();
        $result = $conn->query("SELECT EXISTS (SELECT * FROM employees WHERE email='$email');");
        $row = mysqli_fetch_row($result);

        if ($row[0] == "1") {

            $return = 'Email NO disponible';
            return $return;
        } else {
            $return = 'Email disponible';
            return $return;
        }
    }
}
?>