<?php require_once 'includes/init.php'; ?>
<?php
if (
    !isset($_POST['token'])
    || !isset($_SESSION['token'])
    || empty($_POST['token'])
    || $_POST['token'] !== $_SESSION['token']
) {
    die('token incorrecto');
}

foreach ($_POST as $key => $value) {
    $_POST[$key] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
}

switch ($_POST['task']) {
    case 'check':
        checkEmail($_POST['email']);
        break;
}

function checkEmail($email)
{
    try {
        $response = Employee::checkEmployeeEmail($email);
        echo $response;
        setcookie("email", filter_var($_POST["email"], FILTER_SANITIZE_STRING), time() + (60 * 60 * 24));
    } catch (Exception $error) {
        http_response_code(500);
        echo 'Excepción: ',  $error->getMessage(), "\n";
    }
}
?>