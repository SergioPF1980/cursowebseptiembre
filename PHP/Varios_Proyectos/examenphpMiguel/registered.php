<?php 
require_once 'includes/init.php'; 

if (
    !isset($_POST['token'])
    || !isset($_SESSION['token'])
    || empty($_POST['token'])
    || $_POST['token'] !== $_SESSION['token']
) {
    die('token incorrecto');
} else {
    unset($_SESSION['token']);
}
?>

<?php
    foreach ($_POST as $key => $value) {
		$_POST[$key] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
	}

    $newEmployee = new Employee($_POST);
	$newEmployee->addEmployee();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon/favicon-32x32.png" type="image/png" />
    <title>Registrado</title>
    
</head>

<body>
    <p>Has introducido <?php echo filter_var($_POST["name"], FILTER_SANITIZE_STRING) ?> como nuevo empleado, con <?php echo filter_var($_POST["email"], FILTER_SANITIZE_STRING) ?> como email y 
    <?php echo filter_var($_POST["role"], FILTER_SANITIZE_STRING) ?> como ROL.</p>
    <a href="index.php">Volver</a>
</body>

</html>