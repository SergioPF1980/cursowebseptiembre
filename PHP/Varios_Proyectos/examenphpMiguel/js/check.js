window.addEventListener('load', init);

function init() {
	check.addEventListener('submit', checkEmail);
}

function checkEmail(evt) {
    evt.preventDefault();
	var email = check.email.value;
	var token = check.token.value;
	var formData = new FormData();
	formData.append('email', email);
	formData.append('token', token);
	formData.append('task', 'check');

	fetch('userRequest.php', {
		method: 'POST',
		body: formData
	})
		.then((response) => {
			if (response.ok) {
				return response.text();
			} else {
				throw new Error('Petición fallida');
			}
		})
		.then((message) => {
			showSnackBar(message);
		})
		.catch((error) => {
			showSnackBar(error.message, true);
		});
}

function showSnackBar(message, isError = false) {
	var snackbar = document.getElementById('snackbar');
	snackbar.innerHTML = message;
	snackbar.className = 'show';
	if (isError) {
		snackbar.classList.add('error');
	}
	setTimeout(() => {
		snackbar.className = snackbar.className.replace('show', '');
	}, 3000);
}