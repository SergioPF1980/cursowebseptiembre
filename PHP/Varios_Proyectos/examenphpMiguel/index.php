<?php
require_once 'includes/init.php';

$salt = "3nDuR3&sUrV1v3";
$token = sha1(mt_rand(1, 1000000) . $salt);
$_SESSION['token'] = $token;
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>Examen</title>
</head>

<body>

<h1>Tu empleo</h1>
<h2>Gestión de empleados</h2>
<a class="button" href="#miModal">Añadir empleado</a>

<div id="miModal" class="modal">
	<div class="modal-contenido">
		<a href="#">X</a>
		<h2>Añade un empleado</h2>
		<form name="maint" id="maint" class="sign-up-htm" action="registered.php" method="post">
			<div class="group">
				<label for="name" class="label">Name: </label>
				<input type="text" id="name" name="name" autofocus />
			</div>
			<div class="group">
				<label for="email" class="label">Email: </label>
				<input id="email" type="text" name="email" />
			</div>
			<div class="group">
				<label for="role" class="label">Role: </label>
				<input id="role" name="role" type="text">
			</div>
			<div class="group">
				<button type="submit" class="button">Crear</button>
                <input type='hidden' name='token' value='<?php echo $token; ?>' />
			</div>
		</form>
	</div>
</div>


    <?php $employees = Employee::getEmployees(); ?>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
            </tr>
        </thead>
        <tbody id="table-body" class="fancy">
            <?php foreach ($employees as $employee) :  ?>
                <tr class="row<?php echo $i++ % 2; ?>" id="row-<?= $employee->getId() ?>">
                    <td id="id-<?= $employee->getId(); ?>">
                        <?= $employee->getId(); ?>
                    </td>
                    <td id="name-<?= $employee->getId(); ?>">
                        <?= $employee->getName(); ?>
                    </td>
                    <td id="email-<?= $employee->getId(); ?>">
                        <?= $employee->getEmail(); ?>
                    </td>
                    <td id="role-<?= $employee->getId(); ?>">
                        <?= $employee->getRole(); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<br/>
    <a class="button" href="check.php">Comprobar Email</a>
</body>

</html>