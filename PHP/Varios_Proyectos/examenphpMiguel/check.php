<?php
require_once 'includes/init.php';

$salt = "3nDuR3&sUrV1v3";
$token = sha1(mt_rand(1, 1000000) . $salt);
$_SESSION['token'] = $token;

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/check.js"></script>
    <title>Check mail</title>
</head>

<body>
    <a class="button" href="index.php">Volver</a>

    <h1>Tu empleo</h1>
    <h2>Disponibilidad de email</h2>

    <form name="check">
        
            <label for="email" class="label">Email: </label><br/>
            <input id="email" name="email" type="text" value="<?php echo (empty($_COOKIE["email"])) ? "" : $_COOKIE["email"]; 
            ?>"><br/><br/>

            <button type="submit">Comprobar</button>
            <input type='hidden' name='token' value='<?php echo $token; ?>' />
 
    </form>

    <div id="snackbar"></div>
</body>

</html>