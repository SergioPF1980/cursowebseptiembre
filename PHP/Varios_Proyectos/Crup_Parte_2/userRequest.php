<?php
	include_once 'includes/init.php';
?>
<?php
	if (!checkToken($_POST['token']))	{
		die('token incorrecto');
	}
	foreach ($_POST as $key => $value) {
		$_POST[$key] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
	}
	// unset($_SESSION['token']);
	switch($_POST['task']) {
		case 'update':
			updateUser($_POST);
			break;
		case 'delete':
			deleteUser($_POST['id']);
			break;
	}

	function deleteUser($userId) { 
		try {
			$response = User::deleteUserById($userId);
			echo $response;
		} catch (Exception $error) {
			http_response_code(500);
			echo 'Excepción: ',  $error->getMessage(), "\n";
		}
	}
	
	function updateUser($params) {
		try {
			$user = new User($params);
			$response = $user->updateUser();
			echo $response;
		} catch (Exception $error) {
			http_response_code(500);
			echo 'Excepción: ',  $error->getMessage(), "\n";
		}
	}

	/*function addNewUser($params) {
		$newUser = new User($params);
		echo $newUser->addRecord();
	}*/
?>