<?php
function loadContent($where, $default=''){
		$content=filter_input(INPUT_GET,$where, FILTER_SANITIZE_STRING);
		$default=filter_var($default, FILTER_SANITIZE_STRING);
		$content=(empty($content)) ? $default : $content;

		if($content)
		{
			$html=include 'content/'.$content.'.php';
			return $html;
		}
}
?>