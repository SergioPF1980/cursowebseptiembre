<?php
    class Database {
		private static $_mysqlUser='miguel';
		private static $_mysqlPass='1234';
		private static $_mysqlDb='top_comics';
		private static $_hostName='localhost';
		private static $_connection=NULL;
							
		private function __construct() { }

		public static function getConnection() {
            if (!self::$_connection)
            {
                self::$_connection = new mysqli(self::$_hostName, self::$_mysqlUser,
				self::$_mysqlPass,self::$_mysqlDb);
                if (self::$_connection->connect_error) { 
                    die('Error de conexión: ' . self::$_connection->connect_error); 
                }
            }
            return self::$_connection;
        }

        public static function prep($valor){
            // Escapa caracteres especiales para prevenir inyecciones SQL 
            $valor = self::$_connection->real_escape_string($valor);
            return $valor;	
        }
    }
