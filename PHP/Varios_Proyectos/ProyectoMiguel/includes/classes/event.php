<?php

class Event
{
	protected $evt_name;
	protected $evt_date;
	protected $id_evt;

	public function __construct($entrada = false)
	{
		if (is_array($entrada)) {
			foreach ($entrada as $clave => $valor) {
				$this->$clave = $valor;
			}
		}
	}

	public function getEventName()
	{
		return $this->evt_name;
	}

	public function getEventDate()
	{
		return $this->evt_date;
	}

	public function getEventId()
	{
		return $this->id_evt;
	}

	public function addEvent()
	{

		if ($this->_verifyInput()) {
			$connection = Database::getConnection();

			$query = "INSERT INTO events(evt_name, evt_date)
						VALUES ('" . Database::prep($this->evt_name) . "', '" . Database::prep($this->evt_date) . "')";

			if ($connection->query($query)) {
				$return = array('', 'Evento añadido correctamente');
				return $return;
			} else {
				$return = 'No se ha podido añadir el usuario a la BD.';
				return $return;
			}
		} else {
			$return = 'No se ha añadido el usuario. Falta información obligatoria.';
			return $return;
		}
	}

	protected function _verifyInput()
	{
		$error = false;
		if (!trim($this->evt_name)) {
			$error = true;
		}
		return !$error;
	}


	public static function getEvents()
	{
		$connection = Database::getConnection();
		$query = "SELECT * FROM events";
		if (!$result = $connection->query($query)) {
			throw new Exception('Error en la consulta: ' . $connection->error);
		} else {
			$items = [];
			while ($fetchResult = $result->fetch_object('Event')) {
				$items[] = $fetchResult;
			}
			return $items;
		}
	}

	public function updateEvent()
	{
		if (trim($this->id) && $this->_verifyInput()) {
			$connection = Database::getConnection();

			$query = "UPDATE events SET evt_name='$this->evt_name', evt_date='$this->evt_date' WHERE id_evt='$this->id_evt'";

			if ($connection->query($query)) {
				return 'Usuario actualizado correctamente';
			} else {
				throw new Exception(mysqli_error($connection));
			}
		} else {
			throw new Exception('Datos incorrectos.');
		}
	}

	public static function deleteEventById($eventId)
	{
		$connection = Database::getConnection();
		$query = "DELETE FROM events WHERE id_evt = $eventId";
		if ($connection->query($query)) {
			$return = 'Evento borrado correctamente';
			return $return;
		} else {
			throw new Exception('No se ha borrado el evento de la BD.');
		}
	}
}
?>