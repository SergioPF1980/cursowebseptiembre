<?php

class User
{
	protected $name_usr;
	protected $pass_usr;
	protected $email;
	protected $id;
	/* protected $position;
				protected $phone;
				 */

	public function __construct($entrada = false)
	{
		if (is_array($entrada)) {
			foreach ($entrada as $clave => $valor) {
				$this->$clave = $valor;
			}
		}
	}

	public function getFirst_name()
	{
		return $this->name_usr;
	}

	public function getPass()
	{
		return $this->pass_usr;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function getId()
	{
		return $this->id;
	}

	public function addRecord()
	{

		if ($this->_verifyInput()) {
			$connection = Database::getConnection();

			$query = "INSERT INTO users(name_usr, pass_usr, email)
						VALUES ('" . Database::prep($this->name_usr) . "', '" . Database::prep($this->pass_usr) . "',
						'" . Database::prep($this->email) . "')";

			if ($connection->query($query)) {
				$return = array('', 'Usuario añadido correctamente');
				return $return;
			} else {
				$return = 'No se ha podido añadir el usuario a la BD.';
				return $return;
			}
		} else {
			$return = 'No se ha añadido el usuario. Falta información obligatoria.';
			return $return;
		}
	}

	protected function _verifyInput()
	{
		$error = false;
		if (!trim($this->name_usr)) {
			$error = true;
		}
		if (!trim($this->pass_usr)) {
			$error = true;
		}
		return !$error;
	}


	public static function getUsers()
	{
		$connection = Database::getConnection();
		$query = "SELECT * FROM users";
		if (!$result = $connection->query($query)) {
			throw new Exception('Error en la consulta: ' . $connection->error);
		} else {
			$items = [];
			while ($fetchResult = $result->fetch_object('User')) {
				$items[] = $fetchResult;
			}
			return $items;
		}
	}

	public function updateUser()
	{
		if (trim($this->id) && $this->_verifyInput()) {
			$connection = Database::getConnection();

			$query = "UPDATE users SET name_usr='$this->name_usr', pass_usr='$this->pass_usr', email='$this->email' WHERE id='$this->id'";

			if ($connection->query($query)) {
				return 'Usuario actualizado correctamente';
			} else {
				throw new Exception(mysqli_error($connection));
			}
		} else {
			throw new Exception('Datos incorrectos.');
		}
	}

	public static function deleteUserById($userId)
	{
		$connection = Database::getConnection();
		$query = "DELETE FROM users WHERE id = $userId";
		if ($connection->query($query)) {
			$return = 'Usuario borrado correctamente';
			return $return;
		} else {
			throw new Exception('No se ha borrado el usuario de la BD.');
		}
	}
}
?>