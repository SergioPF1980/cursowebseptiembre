<?php 


	error_reporting(E_ALL);
	ini_set('display_errors', 'On'); 

	session_start();
	require_once 'includes/functions.php';
	

	spl_autoload_register(
		function($class) {
			require_once 'includes/classes/' .strtolower($class) . '.php';
		}
	);
?>