<?php 
require_once 'includes/init.php'; 

if (
    !isset($_POST['token'])
    || !isset($_SESSION['token'])
    || empty($_POST['token'])
    || $_POST['token'] !== $_SESSION['token']
) {
    die('token incorrecto');
} else {
    setcookie("name", filter_var($_POST["name_usr"], FILTER_SANITIZE_STRING), time() + (60 * 60 * 24));
    setcookie("pass", filter_var($_POST["pass_usr"], FILTER_SANITIZE_STRING), time() + (60 * 60 * 24));
    unset($_SESSION['token']);
}
?>

<?php
    foreach ($_POST as $key => $value) {
		$_POST[$key] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
	}

    $newUser = new User($_POST);
	$newUser->addRecord();
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon/favicon-32x32.png" type="image/png" />
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Registered</title>

    <?php
    require_once('includes/init.php');
    require_once('includes/functions.php');
    ?>
    
</head>

<body>
    <p>Has introducido <?php echo filter_var($_POST["name_usr"], FILTER_SANITIZE_STRING) ?> como nombre de usuario
        y <?php echo filter_var($_POST["pass_usr"], FILTER_SANITIZE_STRING) ?> como contraseña.</p>
    <a href="login.php">Volver</a>
</body>

</html>