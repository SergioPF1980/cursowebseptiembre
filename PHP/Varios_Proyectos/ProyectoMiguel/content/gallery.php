<h2 id="galTitle">Galería</h2>
<div id="gallery">
        <img class="img-responsive" src="./images/gallery1.jpg" alt="foto1">
        <img class="img-responsive" src="./images/gallery2.jpg" alt="foto2">
        <img class="img-responsive" src="./images/gallery3.jpg" alt="foto3">
        <img class="img-responsive" src="./images/gallery4.jpg" alt="foto4">
        <img class="img-responsive" src="./images/gallery5.jpg" alt="foto5">
        <img class="img-responsive" src="./images/gallery6.jpg" alt="foto6">
        <img class="img-responsive" src="./images/gallery7.jpg" alt="foto7">
        <img class="img-responsive" src="./images/gallery8.jpg" alt="foto8">
        <img class="img-responsive" src="./images/gallery9.jpeg" alt="foto9">
        <img class="img-responsive" src="./images/gallery10.jpg" alt="foto10">
        <img class="img-responsive" src="./images/gallery11.jpg" alt="foto11">
    </div>