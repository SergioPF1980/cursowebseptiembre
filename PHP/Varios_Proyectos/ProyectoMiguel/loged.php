<?php
require_once 'includes/init.php';

$salt = "3nDuR3&sUrV1v3";
$token = sha1(mt_rand(1, 1000000) . $salt);
$_SESSION['token'] = $token;

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon/favicon-32x32.png" type="image/png" />
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/modal.css" type="text/css">
    <link rel="stylesheet" href="css/tabs.css" type="text/css">
    <script src="js/tabs.js"></script>
    <script src="js/crud.js"></script>
    <title>CRUD</title>
</head>

<body>
    <div id="container">
        <?php include 'content/header.php'; ?>

        <h1>ADMINISTRADOR</h1>

        <div class="tab">
            <button class="tablinks active" onclick="openCity(event, 'Users')">Usuarios</button>
            <button class="tablinks" onclick="openCity(event, 'Events')">Eventos</button>
        </div>

        <div id="Users" class="tabcontent" style="display: block">

            <h2>USUARIOS</h2>
            <?php $users = User::getUsers(); ?>
            <table>
                <thead>
                    <tr>
                        <th>Id: </th>
                        <th>Name: </th>
                        <th>Password: </th>
                        <th>Email: </th>
                        <th>Actions: </th>
                    </tr>
                </thead>
                <tbody id="table-body">
                    <?php foreach ($users as $user) :  ?>
                        <tr id="row-<?= $user->getId() ?>">
                            <td id="id-<?= $user->getId(); ?>">
                                <?= $user->getId(); ?>
                            </td>
                            <td id="name-<?= $user->getId(); ?>">
                                <?= $user->getFirst_name(); ?>
                            </td>
                            <td id="pass-<?= $user->getId(); ?>">
                                <?= $user->getPass(); ?>
                            </td>
                            <td id="email-<?= $user->getId(); ?>">
                                <?= $user->getEmail(); ?>
                            </td>
                            <td id="actions-<?= $user->getId(); ?>">
                                <button id="view-btn-<?= $user->getId(); ?>" data-id="<?= $user->getId() ?>" data-name="<?= $user->getFirst_name() ?>" data-pass="<?= $user->getPass() ?>" data-email="<?= $user->getEmail() ?>">
                                    Ver
                                </button>
                                <button id="delete-btn-<?= $user->getId() ?>" class="delete-btn" data-id="<?= $user->getId() ?>">
                                    Borrar
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>


            <div id="update-modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span id="update-close-modal" class="close">&times;</span>
                    <form name="updateForm">
                        <label for="name_usr">Name: </label>
                        <br />
                        <input type="text" id="name_usr" name="name_usr" value="" />
                        <br />
                        <label for="pass_usr">Pass: </label><br />
                        <input type="text" name="pass_usr" value="" />
                        <br />
                        <label for="email">Email: </label><br />
                        <input type="text" name="email" value="" />
                        <br />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="token" value="<?= $token ?>" />
                        <button type="submit">Actualizar</button>
                    </form>
                </div>
            </div>

            <div id="delete-modal" class="modal">
                <div class="modal-content">
                    <span id="delete-close-modal" class="close" title="Cerrar">&times;</span>
                    <form name="deleteForm">
                        <div class="container">
                            <h2>Borrar usuario</h2>
                            <p>¿Estas seguro de que quieres borrar el usuario?</p>
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="token" value="<?= $token ?>" />
                            <div>
                                <button type="button" id="delete-cancel-modal">Cancelar</button>
                                <button type="submit" class="delete-btn">Borrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="snackbar"></div>
        </div>
        <div id="Events" class="tabcontent">
            
        <h2>EVENTOS</h2>
            <?php $events = Event::getEvents(); ?>
            <table>
                <thead>
                    <tr>
                        <th>Id: </th>
                        <th>Name: </th>
                        <th>Date: </th>
                        <th>Actions: </th>
                        <!--disculpen-->
                    </tr>
                </thead>
                <tbody id="table-body">
                    <?php foreach ($users as $user) :  ?>
                        <tr id="row-<?= $user->getId() ?>">
                            <td id="id-<?= $user->getId(); ?>">
                                <?= $user->getId(); ?>
                            </td>
                            <td id="name-<?= $user->getId(); ?>">
                                <?= $user->getFirst_name(); ?>
                            </td>
                            <td id="pass-<?= $user->getId(); ?>">
                                <?= $user->getPass(); ?>
                            </td>
                            <td id="email-<?= $user->getId(); ?>">
                                <?= $user->getEmail(); ?>
                            </td>
                            <td id="actions-<?= $user->getId(); ?>">
                                <button id="view-btn-<?= $user->getId(); ?>" data-id="<?= $user->getId() ?>" data-name="<?= $user->getFirst_name() ?>" data-pass="<?= $user->getPass() ?>" data-email="<?= $user->getEmail() ?>">
                                    Ver
                                </button>
                                <button id="delete-btn-<?= $user->getId() ?>" class="delete-btn" data-id="<?= $user->getId() ?>">
                                    Borrar
                                </button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>


            <div id="update-modal" class="modal">
                <!-- Modal content -->
                <div class="modal-content">
                    <span id="update-close-modal" class="close">&times;</span>
                    <form name="updateForm">
                        <label for="name_usr">Name: </label>
                        <br />
                        <input type="text" id="name_usr" name="name_usr" value="" />
                        <br />
                        <label for="pass_usr">Pass: </label><br />
                        <input type="text" name="pass_usr" value="" />
                        <br />
                        <label for="email">Email: </label><br />
                        <input type="text" name="email" value="" />
                        <br />
                        <input type="hidden" name="id" value="" />
                        <input type="hidden" name="token" value="<?= $token ?>" />
                        <button type="submit">Actualizar</button>
                    </form>
                </div>
            </div>

            <div id="delete-modal" class="modal">
                <div class="modal-content">
                    <span id="delete-close-modal" class="close" title="Cerrar">&times;</span>
                    <form name="deleteForm">
                        <div class="container">
                            <h2>Borrar usuario</h2>
                            <p>¿Estas seguro de que quieres borrar el usuario?</p>
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="token" value="<?= $token ?>" />
                            <div>
                                <button type="button" id="delete-cancel-modal">Cancelar</button>
                                <button type="submit" class="delete-btn">Borrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div id="snackbar"></div>
        </div>
    </div>
</body>