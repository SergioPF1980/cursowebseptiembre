<?php
require_once 'includes/init.php';

$salt = "3nDuR3&sUrV1v3";
$token = sha1(mt_rand(1, 1000000) . $salt);
$_SESSION['token'] = $token;
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon/favicon-32x32.png" type="image/png" />
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/styleLog.css" type="text/css">
    <title>Log in</title>
    <?php

    ?>
</head>

<body>
    <div id="container">

        <?php include 'content/header.php'; ?>

        <div class="login-wrap">
            <div class="login-html">
                <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Iniciar Sesión</label>
                <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Registrarse</label>
                <div class="login-form">
                    <form class="sign-in-htm" action="loged.php" method="post">

                        <div class="group">
                            <label for="name_usr-log" class="label">Usuario: </label>
                            <input type="text" id="name_usr-log" name="name_usr-log" class="input" value="<?php
                                                                                                            echo (empty($_COOKIE["name"])) ? "" : $_COOKIE["name"];
                                                                                                            ?>" autofocus />
                        </div>

                        <div class="group">
                            <label for="pass_usr-log" class="label">Contraseña: </label>
                            <input id="pass_usr-log" type="password" name="pass_usr-log" class="input" data-type="password" value="<?php
                                                                                                                                    echo (empty($_COOKIE["pass"])) ? "" : $_COOKIE["pass"];
                                                                                                                                    ?>" />
                        </div>

                        <!-- <div class="group">
					<input id="check" type="checkbox" class="check" checked>
					<label for="check"><span class="icon"></span> Keep me Signed in</label>
				</div> -->

                        <div class="group">
                            <button type="submit" class="button">Enviar</button>

                        </div>
                        <div class="hr"></div>
                        <!-- <div class="foot-lnk">
					<a href="#forgot">Forgot Password?</a>
				</div> -->
                    </form>

                    <form name="maint" id="maint" class="sign-up-htm" action="registered.php" method="post">
                        <div class="group">
                            <label for="name_usr" class="label">Usuario: </label>
                            <input type="text" id="name_usr" name="name_usr" class="input" autofocus />
                        </div>
                        <div class="group">
                            <label for="pass_usr" class="label">Contraseña: </label>
                            <input id="pass_usr" type="password" name="pass_usr" class="input" data-type="password" />
                        </div>
                        <div class="group">
                            <label for="pass" class="label">Repetir contraseña: </label>
                            <input id="pass" type="password" class="input" data-type="password">
                        </div>
                        <div class="group">
                            <label for="email" class="label">Email: </label>
                            <input id="email" name="email" type="text" class="input">
                        </div>
                        <div class="group">
                            <button type="submit" class="button">Enviar</button>
                            <!-- <input type="hidden" name="task" id="task" value="regiter" /> -->
                            <input type='hidden' name='token' value='<?php echo $token; ?>' />
                        </div>
                        <div class="hr"></div>
                        <div class="foot-lnk">
                            <label for="tab-1">¿Ya eres miembro?</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>