<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="favicon/favicon-32x32.png" type="image/png" />
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <title>Proyecto</title>
    <?php
    require_once('includes/init.php');
    require_once('includes/functions.php');
    ?>
</head>

<body>
    <div id="container">

        <?php include 'content/header.php'; ?>

        <div class="content">
            <?php
            loadContent('content', 'home');
            ?>
        </div>
    </div>
</body>

</html>