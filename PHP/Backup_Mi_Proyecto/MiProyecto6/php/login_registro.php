<?php
    session_start();

    if(isset($_SESSION['usuario'])){
        header("location: home.php");
    }
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/login_registro.css">
    <title>Login Registro</title>
</head>
<body>
    <main>
        <div class="header">
            <img class="header" src="../imagenes/cabecera.jpg" alt="">
        </div>
        <div class="contenedor__todo">
            <div class="caja__trasera">
                <div class="caja__trasera-login">
                    <h3>¿Tienes una cuenta?</h3>
                    <p>Inicia sesión para entrar a la página</p>
                    <button id="btn__iniciar-sesion">Login</button>
                </div>
                <div class="caja__trasera-register">
                    <h3>¿Tienes una cuenta?</h3>
                    <p>Inicia sesión para entrar a la página</p>
                    <button id="btn__registrarse">Registrarse</button>
                </div>
            </div>
            <div class="contenedor__login-register">
                <!-- Login -->
                <form method="POST" action="login_usuario.php" class="formulario__login">
                    <h2>Iniciar Sesión</h2>
                    <input type="text" placeholder="Correo Electrónico" name="email">
                    <input type="password" placeholder="Contraseña" name="password">
                    <button>Entrar</button>
                </form>
                <!-- Registro -->
                <form method="POST" action="registro_usuario.php" class="formulario__register">
                    <h2>Registrarse</h2>
                    <input type="text" placeholder="Nombre Completo" name="nombre">
                    <input type="text" placeholder="Correo Electrónico" name="email">
                    <input type="text" placeholder="Usuario" name="usuario">
                    <input type="password" placeholder="Contraseña" name="password">
                    <button>Registrarse</button>
                </form>
            </div>
        </div>
    </main>
    <script src="../js/main.js"></script>
</body>
</html>