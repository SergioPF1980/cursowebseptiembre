<?php
    session_start();

    if(isset($_SESSION['usuario'])){
        header("location: home.php");
    }

    $item = new Usuario();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/login_registro.css">
    <title>Login Registro</title>
</head>
<body>
    <main>
        <div class="header">
            <!-- <img class="header" src="../imagenes/cabecera.jpg" alt=""> -->
        </div>
        <div class="contenedor__todo">
            <div class="caja__trasera">
                <div class="caja__trasera-login">
                    <h3>¿Ya tienes una cuenta?</h3>
                    <p>Inicia sesión para entrar en la página</p>
                    <button id="btn__iniciar-sesion">Inicia Sesión</button>
                </div>
                <div class="caja__trasera-register">
                    <h3>¿Aún no tienes una cuenta?</h3>
                    <p>Registrate para que puedas iniciar sesion</p>
                    <button id="btn__registrarse">Registrarse</button>
                </div>
            </div>
            <!-- Formulario de Login y Registro -->
            <div class="contenedor__login-register">
                <!-- Login -->
                <form method="POST" action="../includes/login_usuario.php" class="formulario__login">
                    <h2>Iniciar Sesión</h2>
                    <input type="text" placeholder="Correo Electrónico" name="correo">
                    <input type="password" placeholder="Contraseña" name="contrasena">
                    <button>Entrar</button>
                </form>
                <!-- Registro -->
                <form method="POST" action="../includes/registro_usuario.php" class="formulario__register">
                    <h2>Registrarse</h2>
                    <input type="text" placeholder="Nombre Completo" name="nombre_completo">
                    <input type="text" placeholder="Correo Electrónico" name="correo">
                    <input type="text" placeholder="Usuario" name="usuario">
                    <input type="password" placeholder="Contraseña" name="contrasena">

                    <?php
                        // crea token
                        $salt = 'SomeSalt';
                        $token = sha1(mt_rand(1,1000000) . $salt);
                        $_SESSION['token'] = $token;
                    ?>
                    <input type="hidden" name="task" id="task" value="contact.maint" />
                    <input type='hidden' name='token' value='<?php echo $token; ?>'/>
                    <button>Registrarse</button>
                </form>
            </div>
        </div>
    </main>
    <script src="../js/main.js"></script>
</body>
</html>