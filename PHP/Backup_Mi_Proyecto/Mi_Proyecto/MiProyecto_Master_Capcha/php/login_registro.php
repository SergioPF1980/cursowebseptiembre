<?php
    session_start();

    if(isset($_SESSION['usuario'])){
        header("location: home.php");
    }

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <link rel="stylesheet" href="../css/login_registro.css">
    
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css">
    
    <script src="../js/bootstrap/bootstrap.bundle.min.js"></script>
    <title>Login Registro</title>
</head>
<body>
    <main>
        <?php
        
            if(isset($errors)){
                if(count($errors) > 0){
        ?>
            <div class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Datos incorrectos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div>
                            <?php
                                foreach($errors as $error){
                                    echo $error . '<br>';
                                }
                            ?>
                        </div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <div>
                    </div>
                </div>
            </div>
        <?php 
                }
            }
        ?>
        <div class="header">
            <!-- <img class="header" src="../imagenes/cabecera.jpg" alt=""> -->
        </div>
        <div class="contenedor__todo">
            <div class="caja__trasera">
                <div class="caja__trasera-login">
                    <h3>¿Ya tienes una cuenta?</h3>
                    <p>Inicia sesión para entrar en la página</p>
                    <button id="btn__iniciar-sesion">Inicia Sesión</button>
                </div>
                <div class="caja__trasera-register">
                    <h3>¿Aún no tienes una cuenta?</h3>
                    <p>Registrate para que puedas iniciar sesion</p>
                    <button id="btn__registrarse">Registrarse</button>
                </div>
            </div>
            <!-- Formulario de Login y Registro -->
            <div class="contenedor__login-register">
                <!-- Login -->
                <form method="POST" action="login_usuario.php" class="formulario__login">
                    <h2>Iniciar Sesión</h2>
                    <input type="text" placeholder="Correo Electrónico" name="correo">
                    <input type="password" placeholder="Contraseña" name="contrasena">
                    <button>Entrar</button>
                </form>
                <!-- Registro -->
                <form method="POST" action="registro_usuario.php" class="formulario__register">
                    <h2>Registrarse</h2>
                    <input type="text" placeholder="Nombre Completo" name="nombre_completo">
                    <input type="text" placeholder="Correo Electrónico" name="correo">
                    <input type="text" placeholder="Usuario" name="usuario">
                    <input type="password" placeholder="Contraseña" name="contrasena">
                    <div class="g-recaptcha" data-sitekey="6Lf1AlkcAAAAAEv6aiZ6FZxeZDhBnKlScRZf-hFy">captcha_password</div>
                        
                    
                    <button>Registrarse</button>
                </form>
            </div>
        </div>
    </main>
    <script src="../js/main.js"></script>
</body>
</html>