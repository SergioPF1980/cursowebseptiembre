<?php
    class Conexion{
        private $host;
        private $db;
        private $user;
        private $password;
        private $charset;

        // public function __construct(){
        //     $connectionString = "mysql:hos=".$this->host.";dbname=".$this->db.";charset=utf8";
        //     try{
        //         $this->conect = new PDO($connectionString, $this->user,$this->password);
        //         $this->conect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //         echo "Conexion con la BBDD";
        //     }catch(Exception $e){
        //         $this->conect = 'Error de Conexion';
        //         echo "ERROR: ". $e->getMessage();
        //     }
        // }

        public function __construct(){
            $this->host = "localhost";
            $this->db = "miproyecto";
            $this->user = "root";
            $this->password = "";
            $this->charset = "utf8mb4";
        }
        
        public function connect(){
            try {
                $connection ="mysql:host=".$this->host.";dbname=".$this->db.";charset=utf8";
                $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                            PDO::ATTR_EMULATE_PREPARES => false];
                $pdo = new PDO($connection, $this->user,$this->password, $options);
                return $pdo;
            } catch (PDOException $e) {
                print_r("Error connection: " . $e->getMessage());
            }
        }
    }  
    // $conect = new Conexion();  

?>
