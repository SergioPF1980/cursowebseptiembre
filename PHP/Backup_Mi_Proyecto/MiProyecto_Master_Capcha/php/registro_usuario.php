<?php
    
    include_once ('conexion_bd.php');

    
    
    $nombre_completo = $_POST['nombre_completo'];
    $correo = $_POST['correo'];
    $usuario = $_POST['usuario'];
    $contrasena = $_POST['contrasena'];
    //Encriptar Contraseña
    $contrasena = hash('sha512', $contrasena);
    
    $_SESSION['usuario'] = $usuario;
    
    // Trato de Capcha
    $ip = $_SERVER['REMOTE_ADDR'];
    $capcha = $_POST['g-recaptcha-response'];   //  Devuelve la IP para
    $secretkey = "6Lf1AlkcAAAAAJFj18Y-WI1yDko3saj7_5fYHnYL";    // Clave secreta que nos da Google

    // VALIDAR FORMULARIOS
    $errors = array();
    //  Validar Capcha  
    $request = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$capcha&remoteip=$ip");    // Lo convierte en un JASON

    $atributos = json_decode($request, TRUE);

    if(!$atributos['success']){
        $errors[] = 'Verificar Catcha';
    }

    // VALIDACIONES
    if(empty($nombre_completo)){
        $errors[] = 'El campo Nombre Completo es obligatorio';
    }

    if(!filter_var($correo, FILTER_VALIDATE_EMAIL)){
        $errors[] = 'La dirección de correo no es valida';
    }

    if(empty($usuario)){
        $errors[] = 'El campo Usuario es obligatorio';
    }

    if(empty($contrasena)){
        $errors[] = 'El campo Password es obligatorio';
    }
    

    $query = "INSERT INTO usuarios (nombre_completo, correo, usuario, contrasena) 
                VALUES ('$nombre_completo', '$correo', '$usuario', '$contrasena')";

    $conexion = mysqli_connect("localhost", "root", "", "miproyecto");

    // Verificar que el correo del usuario no se repita en la BBDD
    $verificar_correo = mysqli_query($conexion, "SELECT * FROM usuarios WHERE correo='$correo'");

    if(mysqli_num_rows($verificar_correo) > 0){
        echo '
            <script>
                alert ("Este correo ya está registrado, inténtalo con otro diferente.");
                window.location = "login_registro.php";
            </script>
        ';
        exit();
        mysqli_close($conecion);
    }

    // Verificar que el nombre de usuario no se repita en la BBDD
    $verificar_usuario = mysqli_query($conexion, "SELECT * FROM usuarios WHERE usuario='$usuario'");

    if(mysqli_num_rows($verificar_usuario) > 0){
        echo '
            <script>
                alert ("Este usuario ya está registrado, inténtalo con otro diferente.");
                window.location = "login_registro.php";
            </script>
        ';
        exit();
        mysqli_close($conecion);
    }


    $ejecutar = mysqli_query($conexion, $query);

    if($ejecutar){
        echo '
            <script>
                alert ("Usuario insertado exitosamente.");
                window.location = "home.php";
            </script>
        ';
    }else {
        echo '
            <script>
                alert ("Inténtalo de nuevo. Usuario no insertado.");
                window.location = "login_registro.php";
            </script>
        ';
    }

    mysqli_close($conecion);
?>