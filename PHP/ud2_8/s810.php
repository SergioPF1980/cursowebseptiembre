<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s8</title>
</head>
	<body>
	
		<p>
		<?php
			$weather = 'sunny';
			switch ($weather) :
				case 'rainy':
					echo "It will be rainy today. Use your umbrella.";
					break;
				case 'sunny':
					echo "It will be sunny today. Wear your sunglasses.";
					break;
				case 'snowy':
					echo "It will be snowy today. Bring your shovel.";
					break;
				default:
					echo "<p>I don’t know what the weather is doing today.</p>";
			endswitch;
		?>
		</p>
	
	</body>
</html>
