		<?php require_once './includes/classes/contact.php' ?>

		<?php
			$contactList = [
				new Contact('Martha', 'Smith', 'martha@example.com', 'none', '123-123-123'),
				new Contact('George', 'Smith', 'george@example.com', 'hip hop expert for shure'),
				new Contact('Jeff', 'Meyers', 'jeff@example.com', 'Junior', '668-552-233')
			];
		?>

		<h1>About Us</h1>
		<p>We are all happy to be a part of this. Please contact any of us with questions.</p>
		
		<ul class="ulfancy">
			<?php 
				$odd = false;
				foreach($contactList as $contact) : 	
			?>
				<li class="<?= $odd ? 'row0' : 'row1' ?>">
					<h2><?= $contact->getFullName(); ?></h2>
					<p><?= $contact->getRol(); ?></p>
					<p><?= $contact->getEmail(); ?></p>
					<p><?= $contact->getPhone(); ?></p>
				</li>
			<?php 
				$odd = !$odd;
				endforeach 
			?>
		</ul>
