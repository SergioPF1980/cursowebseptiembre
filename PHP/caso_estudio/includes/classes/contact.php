<?php
    class Contact {
        public $name;
        public $surname;
        public $rol;
        public $phone;
        public $email;
        
        public function __construct($newName, $newSurname, $newEmail, $newRol = '', $newPhone = '') {
            $this->name = $newName;
            $this->surname = $newSurname;
            $this->email = $newEmail;
            $this->rol = $newRol;
            $this->phone = $newPhone;
        }

        public function getFullName() {
            return $this->name . ' ' . $this->surname;
        }

        public function getRol() {
            return $this->rol;
        }

        public function getEmail() {
            return $this->email;
        }

        public function getPhone() {
            return $this->phone;
        }

    }