<?php
    class Employe{
        private $id;
        private $name;
        private $email;
        private $role;

        public function __construct($entrada = false){
            if(is_array($entrada)){
                foreach($entrada as $clave=>$valor){
                    $this->$clave=$valor;
                }
            }
        }

        public function getID(){
            return $this->id;
        }

        public function getName(){
            return $this->name;
        }

        public function getEmail(){
            return $this->email;
        }

        public function getRole(){
            return $this->role;
        }

        public static function getContacts(){
            $items=[];
            $connection = Conexion::getConection();

            $query = "SELECT * FROM employees";

            if($result = $connection->query($query)){
                while ($obj = $result->fetch_object('Employe')) {
                    $items[] = $obj;
                }
                $result->close();
            }else{
                return false;
            }

            return $items;
        }

        // Añadir Employee
        public function getAddRecord(){
            if($this->_verifyInput()){
                $connection = Conexion::getConection();

                $query = "INSERT INTO employees(name, email, role)
                VALUES ('" . Conexion::prep($this->name) . "', 
                        '" . Conexion::prep($this->email) . "',
                        '" . Conexion::prep($this->role) . "')";

                if($connection->query($query)){
                    $result = array('Employe Add');
                    return $result;
                }else{
                    $result = array('Could not add employee.');
                    return $result;
                }
            }else{
                
                $return = array('Could not add employee.');
                return $return;
            }
        }

        // Verify Email
        public static function verifyEmail($email){
            $conexion = Conexion::getConection();
            $queryEmail = $conexion->query("SELECT EXISTS (SELECT * FROM employees WHERE email='$email');");

            $row = mysqli_fetch_row($queryEmail);
            if($row[0] == "1"){
                $result = 'Email is not Valid';
                return $result;
            }else{
                $result = "Email is not in the BBDD";
                return $result;
            }
        }

        protected function _verifyInput() {
            $error = false;
                if (!trim($this->name)) {
                    $error = true;
                }
                if (!trim($this->email)) {
                    $error = true;
                }
                if (!trim($this->role)) {
                    $error = true;
                }
                return !$error;
        }
    }
?>