<?php
    class Conexion{
        // Propiedades protegidas
        private static $_username='root';
        private static $_password='';
        private static $_database='tu_empleo';
        private static $_servername='localhost';

        private static $_connection=null;

        private function __construct(){

        }

        public static function getConection(){
            if(!self::$_connection){
                self::$_connection= new mysqli(self::$_servername, self::$_username,
				self::$_password,self::$_database);
            }
            if (self::$_connection->connect_error){
                die('Error de conexión: ' . self::$_connection->connect_error); 
            }

            return self::$_connection;
        }

        public static function prep($valor){
            // Escapa caracteres especiales para prevenir inyecciones SQL 
            $valor = self::$_connection->real_escape_string($valor);
            return $valor;	
        }

        public function __clone(){

            trigger_error('No se puede clonar',E_USER_ERROR);
        }
        
    }    
?>