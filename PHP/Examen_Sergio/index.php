<?php
    include_once 'includes/init.php';

    // crea token
    $token = createToken();
    $_SESSION['token'] = $token;
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Gestión Empleados</title>
</head>
<body>
    <h1>Tu empleo</h1>
    <h5>Gestion de empleados</h5>

    <div class="w3-container">
        <button onclick="document.getElementById('id01').style.display='block'"
            class="w3-button w3-green w3-large">Añadir Empleado</button>
            <a href="email.php"><input type="button" value="Comprobar Email"></a>

        <div id="id01" class="w3-modal">
            <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

                <div class="w3-center"><br>
                    <span onclick="document.getElementById('id01').style.display='none'"
                        class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
                    <h5>Add Employee</h5>
                </div>

                <form class="w3-container" action="includes/register.php" method="POST">
                    <div class="w3-section">
                        
                        <label><b>Name</b></label>
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Name"
                            name="name" value="<?php isset($_POST["name"]) ? $_COOKIE["name"] : '' ?>"  required>
                        <label><b>Email</b></label>
                        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Email"
                            name="email" value="<?php isset($_POST["email"]) ? $_COOKIE["email"] : '' ?>"  required>
                        <label><b>Role</b></label>
                        <input class="w3-input w3-border" type="text" placeholder="Enter Role" 
                            name="role" value="<?php isset($_POST["role"]) ? $_COOKIE["role"] : '' ?>" required>

                        
                        <input class="w3-input w3-border" type="hidden"  name="id">

                        <input type='hidden' name='token' value='<?php echo $token; ?>'/>

                        <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Create</button>
                    
                    </div>
                </form>

                <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
                    <button onclick="document.getElementById('id01').style.display='none'" type="button"
                        class="w3-button w3-red">Cancel</button>
                </div>

            </div>
        </div>
    </div>
    <?php  $items = Employe::getContacts(); ?>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Rol</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach ($items as $item): ?>
                <tr>
                <td><?php echo $item->getId();?></td>
                <td><?php echo $item->getName();?></td>
                <td><?php echo $item->getEmail();?></td>
                <td><?php echo $item->getRole();?></td>
            
            <?php endforeach;?>
            </tr>
        </tbody>
    </table>
</body>
</html>