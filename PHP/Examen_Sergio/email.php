<?php
    include_once 'includes/init.php';

    // crea token
    $token = createToken();
    $_SESSION['token'] = $token;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/style.css">
    <script src="./js/function.js"></script>
    <title>Comprobar Email</title>
</head>
<body>
    <h1>Tu Empleo</h1>
    <h2>Disponibilidar de email</h2>

    <form action="" method="POST" id="emailForm" name="emailForm">
        <label><b>Email: </b></label><br>
        <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Enter Email"
            name="email" value="<?php isset($_POST["email"]) ? $_COOKIE["email"] : '' ?>"  required><br><br>
        <button type="submit" class="w3-button w3-block w3-green w3-section w3-padding" >
            Comprobar
        </button>
    </form>
    <div id="snackbar"></div>
</body>
</html>