<?php session_start(); //Para comenzar la sesión o acceder a una existente, ponemos esto al inicio ?>
<?php
	$salt="234ewdsknldfs2jkladfs"; //Valor sobre el que encriptaremos, debe ser complicado
	$token = sha1(mt_rand(1,1000000) . $salt); //Añadimos un valor entre 1 y 1000000 al anterior, y encriptamos
	$_SESSION['token'] = $token; //Pasamos como variable de sesión el token generado
	//$_SESSION['token'] = "12345"; //Así comprobamos que falla si el token es incorrecto
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s12</title>
</head>
	<body>
		<form action="s1202b.php" method="post">
			<label for="username">Usuario:</label><br />
			<input type="text" id="username" name="username" value=
				<?php
					echo (empty($_COOKIE["nombre"])) ? "vacio" : $_COOKIE["nombre"];
				?>
			/><br />
			<label for="password">Contraseña</label><br />
			<input type="text" name="password"  value=
			<?php
				echo (empty($_COOKIE["pass"])) ? "vacio" : $_COOKIE["pass"];
			?>	
			/><br />
			<button type="submit">Enviar</button>
			<input type='hidden' name='token' value='<?php echo $token; ?>'/>
		</form>
	</body>
</html>
