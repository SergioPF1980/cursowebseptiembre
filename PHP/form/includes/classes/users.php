<?php
			
	class User{
			 	// Propiedades protegidas
			 	//No accesibles desde fuera, sólo por padre o hijos
				protected $id;
				protected $myName;
				protected $pass;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				//Métodos

				//Los siguientes permiten un acceso controlado a las propiedades, 
				//no de forma directa como antes
				public function getId(){
					return $this->id;
				}

				public function getName(){
					return $this->myName;
				}
				
				public function getPass(){
					return $this->pass;
				}

				public static function getUsers(){
					// Conectar a base de datos
					$connection = Database::getConnection();

					$query = "SELECT * FROM users";

					if(!$result = $connection->query($query)){
						return "Error de la consulta" . $connection->error;
					}else{
						$items = [];
						while($fetchresult = $result->fetch_object('User')){
							$items = $fetchresult;
						}
						return $items;
					}

				}
				
				public function addRecord(){
				
					// Verifica los campos
					if ($this->_verifyInput()) {
						$connection = Database::getConnection();
						// Prepara la información
						$query = "INSERT INTO users(name, pass)
						VALUES ('" . Database::prep($this->myName) . "',
								'" . Database::prep($this->pass) . "'
							)";
						// Ejecuta la sentencia MySql
						if ($connection->query($query)) {
							$return = array('', 'User añadido correctamente');
							return $return;
						}
						else {
							// Manda mensaje de error y regresa a contactmaint
							$return = array('No se ha añadido el contacto. No se ha podido añadir a la BD.');
							return $return;
						}
					}
					else {
						// Manda mensaje de error
						$return = array('No se ha añadido el contacto. Falta información obligatoria.');
						return $return;
					}
				}

				public static function deleteUserByID($userId){
					// Conectar a base de datos
					$connection = Database::getConnection();

					$query = "DELETE FROM users WHERE id = $userId";

					if ($connection->query($query)) {
						$return = array('', 'Users borrado correctamente');
						return $return;
					}
					else {
						// Manda mensaje de error y regresa a contactmaint
						$return = array('No se ha borrado el contacto.');
						return $return;
					}
				}

				public function updateUser(){
					// Conectar a base de datos
					if(!trim($this->id) && $this->_verifyInput()){
						$connection = Database::getConnection();
						$query = "UPDATE users SET 
									name = '$this->myName', 
									pass = '$this->pass' 
									WHERE id = '$this->id'";
					}else{
						return "Error ....";
					}
					
					
					$connection = Database::getConnection();
					if ($connection->query($query)) {

						$return = array('Users modificado correctamente');
						return $return;
					}
					else {
						// Manda mensaje de error y regresa a contactmaint
						$return = 'No se ha modificado el contacto. No se ha podido añadir a la BD.' . mysqli_error($connection);
						return $return;
					}
				}
		
				// Devuelve falso si ha habido error
				protected function _verifyInput() {
				$error = false;
					if (!trim($this->myName)) {
						$error = true;
					}
					if (!trim($this->pass)) {
						$error = true;
					}
					return !$error;
				}


				// Devuelve contactos
				// public static function getContacts(){
				// 	$items= array();
				// 	$connection = Database::getConnection();
				// 	$query="select * from contacts";
					
				// 	if ($resultado = $connection->query($query)) {
			  	// 		// Iterar sobre el resultado
				// 		while ($obj = $resultado->fetch_object('Contact')) {
				// 			// Añade objeto al vector
				// 			$items[]=$obj;
				// 		}
				// 	// Libera resultado
				// 	$resultado->close();
				// 	}
				// 	else{
				// 		return false;
				// 	}
				// 	return $items;
				// }

			}
			
?>