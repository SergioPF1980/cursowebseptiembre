<?php

    require_once 'includes/init.php';
    

    // session_start();
    $token = createToken();
    $_SESSION['token'] = $token;
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejemplo Registro</title>
</head>
<body>
    <form action="registrarUsuario.php" method="post">
        <label for="username">Usuario:</label><br />
        <input type="text" id="username" name="username" 
        value="<?php isset($_COOKIE["username"]) ? $_COOKIE["username"] : ''; ?>"/><br />
        <label for="password">Contraseña</label><br />
        <input type="text" name="password"  value="
            <?php
                isset($_COOKIE["password"]) ? $_COOKIE["password"] : '';
            ?>"/><br />

        <input type="hidden" id="token" name="token" value="<?php $token ?>"/><br />
        <button type="submit">Enviar</button>
    </form>
</body>
</html>