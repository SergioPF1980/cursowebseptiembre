<h1>Categories</h1>

		<ul class="ulfancy">

			<li class="row0">
    			<div class="list-description">
    				<h2><a href="index.php?content=gents&sidebar=catnav">Gents</a></h2>
    				<p>Gents' clothing from the 18th century to modern times</p>
    				<a class="button display" href="index.php?content=gents&sidebar=catnav">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>

    		<li class="row1">
    			<div class="list-description">
    				<h2><a href="index.php?content=sporting&sidebar=catnav">Sporting</a></h2>
    				<p>Sporting clothing and gear.</p>
    				<a class="button display" href="index.php?content=sporting&sidebar=catnav">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>

    		<li class="row0">
    			<div class="list-description">
    				<h2><a href="index.php?content=women&sidebar=catnav">Women</a>
    			</h2>
    				<p>Women's Clothing from the 18th century to modern times</p>
    				<a class="button display" href="index.php?content=women&sidebar=catnav">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>
		</ul>
