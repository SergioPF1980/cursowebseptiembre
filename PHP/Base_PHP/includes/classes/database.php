<?php

class Database{
    private static $_mysqlUser = 'mtw';
    private static $_mysqlPass = 'localhost';
    private static $_mysqlDB = 'bd_mtw';
    private static $_hostName = 'localhost';
    private static $_connection = null;

    private function __construct($entrada=false) {

    }

    public static function getConnection(){
        if(!self::$_connection){
            self::$_connection = new mysqli(self::$_hostName, self::$_mysqlUser,
            self::$_mysqlPass, self::$_mysqlDB);

            if(self::$_connection->connect_error){
                die('Error en la base de datos: ' . self::$_connection->connect_error);
            }
        }

        return self::$_connection;
    }
}
    


?>