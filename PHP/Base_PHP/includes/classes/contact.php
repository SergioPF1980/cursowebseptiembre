<?php 
    class Contact{
        //Propiedades
        public $name;
        public $secondName;
        public $position;
        public $email;
        public $phone;

        //Constructor
        public function __construct($name, $secondName, $position, $email, $phone){
            $this->name = $name;
            $this->secondName = $secondName;
            $this->position= $position;
            $this->email = $email;
            $this->phone = $phone;
            
        }

        //Métodos
		public function getFullName()
		{
			return $this->name . ' '. $this->secondName;
		}

        public function getPosition(){
            return $this->position;
        }

        public function getEmail(){
            return $this->email;
        }

        public function getPhone(){
            return $this->phone;
        }
    }
?>
