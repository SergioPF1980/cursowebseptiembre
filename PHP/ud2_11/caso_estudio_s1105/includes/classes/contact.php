<?php
			
	class contact
			 {
			 	// Propiedades
				public $first_name;
				public $last_name;
				public $position;
				public $email;
				public $phone;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				//Métodos
				public function name() //Devuelve el nombre completo del contacto
				{
					echo $this->first_name." ".$this->last_name;
				}				
			}
			
?>