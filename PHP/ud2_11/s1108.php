<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s11</title>
</head>
	<body>
		<?php	
			
			class suma
			{
				/*
					Definición de elementos estáticos
					No requieren la generación de una instancia para ser llamados
				*/
				static $instancias = 0;
				static function info()
				{
					return "Hay ".self::$instancias." instancias";
				}

				static function valores($a=false, $b=false)
				{
					if(is_integer($a)&&is_integer($b))
					{
						$resultado=$a+$b;
						return $resultado;
					}
				}

				/*
					Definición de elementos no estáticos
				*/
				function __construct()
				{
					self::$instancias++; //FORMA DE LLAMAR A UN ELEMENTO ESTÁTICO DENTRO DE LA CLASE
				}
				
			}

			echo suma::info();
			echo '<br />';
			echo suma::valores(3,4);
			$i1=new suma();
			echo '<br />';
			echo suma::info();
			$i2=new suma();
			echo '<br />';
			echo $i2->info();
			$i3=new suma();
			echo '<br />';
			echo suma::info();
			echo '<br />';
			echo $i2->valores(7,1);



		?>
		
	</body>
</html>
