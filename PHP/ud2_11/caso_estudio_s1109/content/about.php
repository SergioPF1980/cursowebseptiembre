		<h1>About Us</h1>
		<p>We are all happy to be a part of this. Please contact any of us with questions.</p>
		

		<?php
		
		$items=array();//Lista de contactos
		
		/*
			Definición de cada uno de los contactos como elemento de $items
		*/
		$items[1]=new contact(array('first_name'=>'Martha','last_name'=>'Smith',
			'position'=>'none','email'=>'martha@example.com','phone'=>''));
		
		$items[2]=new contact(array('first_name'=>'George','last_name'=>'Smith',
			'position'=>'none','email'=>'george@example.com','phone'=>' 515-555-1236'));
		
		$items[3]=new contact(array('first_name'=>'Jeff','last_name'=>'Meyers',
			'position'=>'hip hop expert for shure','email'=>'jeff@example.com','phone'=>''));
		
		$items[4]=new contact(array('first_name'=>'Peter','last_name'=>'Meyers',
			'position'=>'none','email'=>'peter@example.com','phone'=>'515-555-1237'));
		
		$items[5]=new contact(array('first_name'=>'Sally','last_name'=>'Smith',
			'position'=>'none','email'=>'sally@example.com','phone'=>'515-555-1235'));
		
		$items[6]=new contact(array('first_name'=>'Sarah','last_name'=>'Finder',
			'position'=>'Lost Soul','email'=>'finder@a.com','phone'=>'555-123-5555'));
	
		?>

		<ul class="ulfancy">

		<?php
		
			for($i=0; $i<count($items);$i++) 
			{
				$aux=$i%2;
				//echo "<br /> aux1 vale ".$aux1." -- aux2 vale".$aux2."<br />";
				echo '<li class="row'.$aux.'"><h2>';//OJO - Pongo h2 aquí porque si lo pongo en el inicio de la anterior, pone el nombre antes que el h2
				echo $items[$i+1]->name().'</h2><p>';
				echo 'Position: '.$items[$i+1]->getPosition().'<br />';
				echo 'Email: '.$items[$i+1]->getEmail().'<br />';
				echo 'Phone: '.$items[$i+1]->getPhone().'<br /></p></li>';
				
			}
		
					
		
		//La otra manera de hacerlo es con foreach		
		/*
		$i=0;		
		foreach ($items as $item):?>
			<li class=<?php echo "row".$i++ % 2; ?>>
				<h2><?php echo $item->name();?></h2>
				<p>Position: <?php echo $item->position;?><br />
				Email: <?php echo $item->email;?><br />
				Phone: <?php echo $item->phone;?><br /></p>
			</li>
		<?php endforeach;
		*/

		?>
		


		
		
		</ul>