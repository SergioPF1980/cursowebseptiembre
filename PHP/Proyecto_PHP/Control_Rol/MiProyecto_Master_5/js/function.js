/* VALIDAR FORMULARIO */
const name = document.getElementById('nombre_completo');
const user = document.getElementById('usuario');
const email = document.getElementById('correo');
const password = document.getElementById('contrasena');

const form = document.getElementById('formulario2');
const parrafo = document.getElementById('warnings');

form.addEventListener('submit', event => {
    event.preventDefault();

    let warnings = "";
    let nameValidation = /^[a-zA-Z]\s*$/;
    let userValidation = /^[a-z0-9_-]{3,16}$/;
    let emailValidation = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    let entrar = false;
    parrafo.innerHTML = "";

    if(name.value.length < 3 && !nameValidation.test(name.value)){
        warnings += `El nombre no es valido </br>`;
        entrar = true; 
    }

    if(!emailValidation.test(email.value)){
        warnings += `El Correo no es valido </br>`;
        entrar = true;
    }

    if(!userValidation.test(user.value)){
        warnings += `El Usuario no es valido </br>`;
        entrar = true; 
    }

    if(password.value.length < 4){
        warnings += `La contraseña tiene que tener 4 caracters </br>`;
        entrar = true;
    }

    if (entrar) {
        parrafo.innerHTML = warnings;
    } else {
        parrafo.innerHTML = "Envio correcto";
    }
});


