<?php
    // include_once 'init.php';
    include_once 'init.php';

    $name = $_POST['name'];
    $email = $_POST['email'];
    $usuario = $_POST['usuario'];
    $password = $_POST['password'];
    //Encriptar Contraseña
    $password = hash('sha512', $password);
    $id_rol = $_POST['id_rol'];
    $id_rol = 2;

    $data = [
        'name' => $name,
        'email' => $email,
        'usuario' => $usuario,
        'password' => $password,
        'id_rol' => $id_rol
    ];

    $addRegister = new Usuario($data);

    $result = $addRegister->addRecord();

    echo $result['content'];
