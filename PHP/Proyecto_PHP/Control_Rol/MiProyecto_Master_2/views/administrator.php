<?php

    include_once '../includes/init.php';

    $usuario = $_SESSION['usuario'];
    

    // $prueba = Usuario::getUsuario($usuario);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/menu.css">
    <link rel="stylesheet" href="../css/home.css">
    <script scr="../js/validar_login.js"></script>
    <title>Administrator</title>
</head>
<body>
<nav class="nav">
        <div class="nav__container">

            <!-- <h1 class="nav__logo">Sergio</h1> -->
            <p>Administrador</p>
            <label for="menu" class="nav__label">
                <img src="../images/menu.svg" alt="" class="nav__img">
            </label>
            <input type="checkbox" id="menu" class="nav__input">

            <div class="nav__menu">
                <a href="../index.php" class="nav__item">Inicio</a>
                <a href="../includes/cerrar_sesion.php" class="nav__item">Log out</a>
            </div>
        </div>
    </nav>
    <img class="header" src="../images/cabecera.jpg" alt="">
    <h1>Bienveido Administrador!</h1>
</body>
</html>