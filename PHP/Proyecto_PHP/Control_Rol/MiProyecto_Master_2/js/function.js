/* VALIDAR FORMULARIO */

function validateForm() {
    var errors = [];
    var name = document.getElementById('name').value.trim();
    var user = document.getElementById('usuario').value.trim();
    
    if(!name) {
        errors.push('El campo Nombre Completo es obligatorio');
    }

    if(!user) {
        errors.push('El campo Usuario es obligatorio');
    }
    
    var email = document.getElementById('correo').value.trim();
    if(!(/[\w]+@{1}[\w]+\.[a-z]{2,3}/.test(email))) {
        errors.push('El campo Email no tiene formato correcto');
        // return false;
    }
    
    if(errors.length > 0) {
        showErrors(errors);
        return false;
    }
    return true;
}

function showErrors(formErrors) {
    var errorsContent = document.getElementById('errors-content');
    errorsContent.innerHTML = ''; 
    errorsContent.className = ''; 
    for(var i in formErrors) {
        var tagP = document.createElement('p');
        tagP.innerHTML = formErrors[i]; 
        errorsContent.appendChild(tagP);
    }
    errorsContent.className = 'error'; 
}