<?php
    // session_start();
    // include_once '../includes/function.php';
    require_once '../includes/init.php';

    // crea token
    // $token = createToken();
    // $_SESSION['token'] = $token;

    if(isset($_SESSION['usuario'])){
        header("location: home.php");
    }

    $item = new Usuario();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/login_registro.css">
    <link rel="stylesheet" href="../css/snackbar.css">
    <script scr="../js/validar_login.js"></script>
    <title>Login Registro</title>
</head>
<body>
    <main>
        <div class="header">
            <!-- <img class="header" src="../imagenes/cabecera.jpg" alt=""> -->
        </div>
        <div class="contenedor__todo">
            <div class="caja__trasera">
                <div class="caja__trasera-login">
                    <h3>¿Ya tienes una cuenta?</h3>
                    <p>Inicia sesión para entrar en la página</p>
                    <button id="btn__iniciar-sesion">Inicia Sesión</button>
                </div>
                <div class="caja__trasera-register">
                    <h3>¿Aún no tienes una cuenta?</h3>
                    <p>Registrate para que puedas iniciar sesion</p>
                    <button id="btn__registrarse">Registrarse</button>
                </div>
            </div>
            <!-- Formulario de Login y Registro -->
            <div class="contenedor__login-register">
                <!-- Login -->
                <form method="POST" action="../includes/login_usuario.php" class="formulario__login" id="formulario">
                    <h2>Iniciar Sesión</h2>
                    <input type="text" placeholder="Correo Electrónico" name="correo" id="correo">
                    <input type="password" placeholder="Contraseña" name="contrasena" id="contrasena">
                    <button type="submit" id="btn-login">Entrar</button>
                    <div id="snackbar"></div>
                </form>

                <!-- Registro -->
                <form method="POST" action="../includes/registro_usuario.php" class="formulario__register">
                    <h2>Registrarse</h2>
                    <input type="text" placeholder="Nombre Completo" name="nombre_completo" value="<?php 
                        isset($_POST["nombre_completo"]) ? $_COOKIE["nombre_completo"] : ''
                    ?>">
                    <input type="text" placeholder="Correo Electrónico" name="correo" value="<?php 
                        isset($_POST["correo"]) ? $_COOKIE["correo"] : ''
                    ?>">
                    <input type="text" placeholder="Usuario" name="usuario" value="<?php 
                        isset($_POST["usuario"]) ? $_COOKIE["usuario"] : ''
                    ?>">
                    <input type="password" placeholder="Contraseña" name="contrasena" value="<?php 
                        isset($_POST["contrasena"]) ? $_COOKIE["contrasena"] : ''
                    ?>">
                    <input type="hidden" name="rol" value="<?php 
                        isset($_POST["rol"]) ? $_COOKIE["rol"] : ''
                    ?>">

                    <!-- <input type="hidden" name="task" id="task" value="contact.maint" /> -->
                    <input type='hidden' name='token' value='<?php echo $token; ?>'/>
                    <button>Registrarse</button>
                </form>
            </div>
        </div>
    </main>
    <script src="../js/main.js"></script>
</body>
</html>