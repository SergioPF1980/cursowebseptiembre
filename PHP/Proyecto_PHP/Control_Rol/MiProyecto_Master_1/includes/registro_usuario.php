<?php
    // include_once 'init.php';
    include_once 'init.php';

    $nombre = $_POST['nombre_completo'];
    $correo = $_POST['correo'];
    $usuario = $_POST['usuario'];
    $contrasena = $_POST['contrasena'];
    //Encriptar Contraseña
    $contrasena = hash('sha512', $contrasena);
    $rol = $_POST['rol'];
    $rol = 2;

    $data = [
        'name' => $nombre,
        'email' => $correo,
        'usuario' => $usuario,
        'password' => $contrasena,
        'rol' => $rol
    ];

    $addRegister = new Usuario($data);

    $result = $addRegister->addRecord();

    echo $result['content'];
