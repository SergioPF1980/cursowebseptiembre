<?php

    include_once '../includes/init.php';

    $usuario = $_SESSION['usuario'];
    

    // $prueba = Usuario::getUsuario($usuario);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/menu.css">
    <link rel="stylesheet" href="../css/home.css">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link  rel="icon"   href="Imagenes/icono.png"  type="image/png"/> -->
    <!-- <link rel="apple-touch-icon" sizes="120x120" href="../Imagenes/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../Imagenes/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../Imagenes/favicon-16x16.png">
    <link rel="manifest" href="../Imagenes/site.webmanifest">
    <link rel="mask-icon" href="../Imagenes/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff"> -->


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <title>Administrator</title>
</head>
<body>
<nav class="nav">
        <div class="nav__container">

            <!-- <h1 class="nav__logo">Sergio</h1> -->
            <p>Administrador</p>
            <label for="menu" class="nav__label">
                <img src="../images/menu.svg" alt="" class="nav__img">
            </label>
            <input type="checkbox" id="menu" class="nav__input">

            <div class="nav__menu">
                <a href="../index.php" class="nav__item">Inicio</a>
                <a href="../includes/cerrar_sesion.php" class="nav__item">Log out</a>
            </div>
        </div>
</nav>
    <img class="header" src="../images/cabecera.jpg" alt="">
    <h1>Bienveido Administrador!</h1>

    <div class="row">
            <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <th>Password</th>
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <?php 
                    $items = array();
                    $items = Usuario::getUsers();
                    foreach($items as $item){ ?>
                    <tr>
                        
                        <td> <?php echo htmlspecialchars($item->getName()); ?> </td>
                        <td> <?php echo $item->getUser(); ?> </td>
                        <td> <?php echo $item->getEmail(); ?> </td>
                        <td> <?php echo $item->getPassword(); ?> </td>
                        <td> <?php echo $item->getRol(); ?> </td>
                        
                        <form action="" method="POST">
                            <input type="hidden" name="txtID" value="<?php echo $item->getId(); ?>" />

                            <input type="submit" value="Seleccionar" class="btn btn-info" name="accion">
                            <button value="btnEliminar" onclick="return Confirmar('¿Quieres borrar el Empleado?');" type="submit" class="btn btn-danger" name="accion">Eliminar</button>
                        </form>
                        </td>

                    </tr>
                <?php } ?>

            </table>
        </div>

</body>
</html>