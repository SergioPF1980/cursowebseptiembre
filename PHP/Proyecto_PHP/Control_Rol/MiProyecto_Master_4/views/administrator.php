<?php

    include_once '../includes/init.php';

    $usuario = $_SESSION['usuario'];
    

    // $prueba = Usuario::getUsuario($usuario);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/menu.css">
    <link rel="stylesheet" href="../css/home.css">
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <title>Administrator</title>
</head>
<body>
    <nav class="nav">
            <div class="nav__container">

                <!-- <h1 class="nav__logo">Sergio</h1> -->
                <p>Administrador</p>
                <label for="menu" class="nav__label">
                    <img src="../images/menu.svg" alt="" class="nav__img">
                </label>
                <input type="checkbox" id="menu" class="nav__input">

                <div class="nav__menu">
                    <a href="../index.php" class="nav__item">Inicio</a>
                    <a href="../includes/cerrar_sesion.php" class="nav__item">Log out</a>
                </div>
            </div>
    </nav>
    <img class="header" src="../images/cabecera.jpg" alt="">
    <h1 class="titulo">Bienvenido Administrador!</h1>
    <h5 class="titulo">Control de Usuarios</h5>
    <div class="container">
        <!-- <h2 class="titulo">Usuarios</h2><br> -->
        <form action="POST">
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="titulo" id="exampleModalLabel">Usuario</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-row">
                                <input type="hidden" required name="id" value="<?php echo $id; ?>" placeholder="" id="id" require="">
                                
                                <div class="form-group col-md-4">
                                    <label for="">Nombre:</label>
                                    <input type="text" class="form-control <?php echo (isset($error['name']))?"is-invalid":""; ?>" name="name" value="<?php echo $name; ?>" placeholder="" id="name" require="">
                                    <div class="invalid-feedback">
                                        <?php echo (isset($error['name']))?$error['name']:""; ?>
                                    </div>
                                    <br>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="">Usuario:</label>
                                    <input type="text" class="form-control <?php echo (isset($error['usuario']))?"is-invalid":""; ?>"  name="usuario" value="<?php echo $txtApellido1; ?>" placeholder="" id="txtApellido1" require="">
                                    <div class="invalid-feedback">
                                        <?php echo (isset($error['usuario']))?$error['usuario']:""; ?>
                                    </div>
                                    <br>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="">Email:</label>
                                    <input type="email" class="form-control" required name="email" value="<?php echo $email; ?>" placeholder="" id="email" require="">
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button value="btnAgregar" <?php echo $accionAgregar; ?> class="btn btn-success" type="submit" name="accion">Agregar</button>
                            <button value="btnModificar" <?php echo $accionModificar; ?> class="btn btn-warning" type="submit" name="accion">Modificar</button>
                            <button value="btnEliminar" onclick="return Confirmar('¿Quieres borrar el Empleado?');" <?php echo $accionEliminar; ?> class="btn btn-danger" type="submit" name="accion">Eliminar</button>
                            <button value="btnCancelar" <?php echo $accionCancelar; ?> class="btn btn-primary" type="submit" name="accion">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-añadir" data-toggle="modal" data-target="#exampleModal">
                    Añadir Registro +
                </button>
                
        </form>
        <div class="row">
            <table class="table table-hover table-bordered">
                <thead class="thead-dark">
                    <tr>
                        <th>Nombre</th>
                        <th>Usuario</th>
                        <th>Correo</th>
                        <!-- <th>Password</th> -->
                        <th>Rol</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <?php 
                    $items = array();
                    $items = Usuario::getUsers();
                    foreach($items as $item){ ?>
                    <tr>
                        
                        <td> <?php echo $item->getName(); ?> </td>
                        <td> <?php echo $item->getUser(); ?> </td>
                        <td> <?php echo $item->getEmail(); ?> </td>
                        <!-- <td> <?php echo $item->getPassword(); ?> </td> -->
                        <td> <?php echo $item->getRol(); ?> </td>
                        <td>
                            <form action="" method="POST">
                                <input type="hidden" name="id" value="<?= $item->getId(); ?>">

                                <input type="submit" value="Seleccionar" class="btn btn-info" name="action"/>
                                <button type="submit" onclick="return Confirmar('¿Quieres borrar el usuario?');" value="btnEliminar" class="btn btn-danger" name="action">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <?php if($mostrarModal){ ?>
            <script>
                $('#exampleModal').modal('show');
            </script>
        <?php } ?>

        <script>
            function Confirmar(Mensaje){
                return (confirm(Mensaje))?true:false;
            }
        </script>
    </div>
</body>
</html>