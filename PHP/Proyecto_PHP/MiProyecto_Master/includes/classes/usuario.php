<?php
    class Usuario{
        protected $name;
        protected $email;
        protected $usuario;
        protected $password;

        //Constructor
        public function __construct($entrada = false){
            if(is_array($entrada)){
                foreach($entrada as $clave=>$valor){
                    $this->$clave=$valor;
                }
            }
        }

        //MÉTODOS
        public function getName(){
            return $this->name;
        }
        
        public function getEmail(){
            return $this->email;
        }
        
        public function getUser(){
            return $this->usuario;
        }
        
        public function getPassword(){
            return $this->password;
        }	
        
        //Método añadir Registro
        public function addRecord(){
            // Verificar los campos del formulario
            if(true){
                /**  
                 * Llamamos al método getConnection de la clase Conexión 
                 * con '::' porque el metodo es static 
                **/
                $conexion = Conexion::getConnection();

                $query = "INSERT INTO usuarios (nombre_completo, correo, usuario, contrasena) 
                VALUES ('".Conexion::prep($this->name)."',  
                        '".Conexion::prep($this->email)."',
                        '".Conexion::prep($this->usuario)."',
                        '".Conexion::prep($this->password)."'
                )";
            }

            // Verificar que el correo del usuario no se repita en la BBDD
            $this->_verifyEmail();

            // Verificar que el nombre de usuario no se repita en la BBDD
            $this->_verifyUser();

            $ejecutar = mysqli_query($conexion, $query);

            if($ejecutar){
                $content= '
                    <script>
                        alert ("Usuario insertado exitosamente.");
                        window.location = "../views/home.php";
                    </script>
                ';
                $ok = true;
            }else {
                $error = mysqli_error($conexion);
                $content= '
                    <script>
                        alert ("Inténtalo de nuevo. Usuario no insertado. '.$error.'");
                        
                    </script>
                ';
                $ok = false;
            }

            $data=[
                'estado'=>$ok,
                
                'content'=>$content,
                ];

            mysqli_close($conexion);
            return $data;
        }

        private function _verifyEmail(){
            $conexion = Conexion::getConnection();
            $queryEmail = "SELECT * FROM usuarios WHERE correo='".Conexion::prep($this->email)."'";

            $verificar_email = mysqli_query($conexion, $queryEmail);

            if(mysqli_num_rows($verificar_email) > 0){
                echo '
                    <script>
                        alert ("Este correo ya está registrado, inténtalo con otro diferente.");
                        window.location = "../views/login_registro.php";
                    </script>
                ';
                exit();
                mysqli_close($conexion);
            }
        }

        private function _verifyUser(){
            $conexion = Conexion::getConnection();
            $queryUsuario = "SELECT * FROM usuarios WHERE usuario='".Conexion::prep($this->usuario)."'";

            $verificar_usuario = mysqli_query($conexion, $queryUsuario);

            if(mysqli_num_rows($verificar_usuario) > 0){
                echo '
                    <script>
                        alert ("Este usuario ya está registrado, inténtalo con otro diferente.");
                        window.location = "../views/login_registro.php";
                    </script>
                ';
                exit();
                mysqli_close($conexion);
            }
        }

        // Método coger valor usuario y mostrarlo por pantalla
		public static function getUsuario($usuario){

			$conexion = Conexion::getConnection();
            $verificar_usuario = mysqli_query($conexion,"SELECT usuario FROM usuarios WHERE correo = '$usuario'");

            if($verificar_usuario){
                $verificar_usuario = $verificar_usuario->fetch_array(MYSQLI_ASSOC);
                return $verificar_usuario['usuario'];
            }
            return '';
		}

        // Devuelve falso si ha habido error
		protected function _verifyInput() {
			$error = false;
			if (!trim($this->name)) {
				$error = true;
			}
			if (!trim($this->email)) {
				$error = true;
			}
            if (!trim($this->usuario)) {
				$error = true;
			}
            if (!trim($this->password)) {
				$error = true;
			}
			return !$error;
		}
    }
?>