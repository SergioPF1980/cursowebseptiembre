<?php
    // include_once 'init.php';
    include_once 'init.php';

    $nombre = $_POST['nombre_completo'];
    $correo = $_POST['correo'];
    $usuario = $_POST['usuario'];
    $contrasena = $_POST['contrasena'];
    //Encriptar Contraseña
    $contrasena = hash('sha512', $contrasena);

    $data = [
        'name' => $nombre,
        'email' => $correo,
        'usuario' => $usuario,
        'password' => $contrasena,
    ];

    $addRegister = new Usuario($data);

    $result = $addRegister->addRecord();

    echo $result['content'];
