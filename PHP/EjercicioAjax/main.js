window.addEventListener('load', init);

function init() {
    var button = document.getElementById('btn-boton'); 
    button.addEventListener('click', () => {

            var formData = new FormData();
            formData.append('a', 3);
            formData.append('b', 6);

            fetch('suma.php',{
                method: 'POST',
                body: formData
                
            }).then((response) => {
                    
                    if(response.ok){
                        return response.text();
                    }else{
                        throw 'Ha habido un error';
                    }
                    
                }).then(info => console.log(info))
                .catch((error) => console.log("Hubo un problema" + error));
                console.log(formData);
            alert('');
    });

    /* 
        FORMA DE HACERLO CON GET
    var button = document.getElementById('btn-boton'); 
    button.addEventListener('click', () => {

            var formData = new FormData();
            formData.append('a', 3);
            formData.append('b', 6);

            fetch('suma.php?a=3&b=4'
                
            ).then((response) => {
                    
                    if(response.ok){
                        return response.text();
                    }else{
                        throw 'Ha habido un error';
                    }
                    
                }).then(info => console.log(info))
                .catch((error) => console.log("Hubo un problema" + error));
                console.log(formData);
            alert('');
    }); */
}