		<h1>Categories</h1>

		<ul class="ulfancy">
			
    		<li class="row0">
    			<div class="list-description">
    				<h2><a href="index.php?content=gents">Gents</a></h2>	
    				<p>Gents' clothing from the 18th century to modern times</p>
    				<a class="button display" href="index.php?content=gents">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>
    			
    		<li class="row1">
    			<div class="list-description">
    				<h2><a href="index.php?content=sporting">Sporting</a></h2>	
    				<p>Sporting clothing and gear.</p>
    				<a class="button display" href="index.php?content=sporting">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>
    			
    		<li class="row0">
    			<div class="list-description">
    				<h2><a href="index.php?content=women">Women</a>
    			</h2>	
    				<p>Women's Clothing from the 18th century to modern times</p>
    				<a class="button display" href="index.php?content=women">Display Lots</a>
    			</div>
    			<div class="clearfloat"></div>
    		</li>
		</ul>
