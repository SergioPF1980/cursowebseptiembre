<?php
    function createToken() {
        $salt = random_bytes(15); //Valor sobre el que encriptaremos, debe ser complicado
        $token = sha1(mt_rand(1,1000000) . $salt); //Añadimos un valor entre 1 y 1000000 al anterior, y encriptamos
        return $token;
    }
    function checkToken($formToken, $sessionToken = false) {
        $sessionToken = $sessionToken === false ? $_SESSION['token'] : $sessionToken;
        return (
            isset($formToken)
            && isset($sessionToken)
            && !empty($formToken)
            && $formToken === $sessionToken
        );	
    }
