<?php
			
	class User {
			 	// Propiedades protegidas
			 	//No accesibles desde fuera, sólo por padre o hijos
				protected $id; 
				protected $name;
				protected $pass;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				public function getId () {
					return $this->id;
				}

				public function getName () {
					return $this->name;
				}

				public function getPass () {
					return $this->pass;
				}

				public static function getUsers() {
					$connection = Database::getConnection();
					$query = "SELECT * FROM users";
					if(!$result = $connection->query($query)) {
						throw new Exception('Error en la consulta: ' . $connection->error);
					}else {
						$items = [];
						while($fetchResult = $result->fetch_object('User')) {
							$items[] = $fetchResult;
						}
						return $items;
					}
				}

				public static function deleteUserById($userId){
					$connection = Database::getConnection();
					$query = "DELETE FROM users WHERE id = $userId";
					if ($connection->query($query)) {
						$return = 'Usuario borrado correctamente';
						return $return;
					}
					else {
						$return = 'No se ha borrado el usuario de la BD.';
						return $return;
					}
				}
				public function updateUser(){
					if(trim($this->id) && $this->_verifyInput()) {
						$connection = Database::getConnection();

						$query = "UPDATE users SET name='$this->name', pass='$this->pass' WHERE id='$this->id'";

						if ($connection->query($query)) {
							return 'Usuario actualizado correctamente';
						}
						else {
							throw new Exception(mysqli_error($connection));
						}

					} else {
						throw new Exception('Datos incorrectos.');
					}
					
					
				}

				public function addRecord() {
					// Verifica los campos
					if ($this->_verifyInput()) {
						$connection = Database::getConnection();
						// Prepara la información
						$query = "INSERT INTO users(name, pass)
						VALUES ('" . Database::prep($this->name)  . "', 
						'" . Database::prep($this->pass) . "')";
						if ($connection->query($query)) {
							$return = 'Usuario añadido correctamente';
							return $return;
						}
						else {
							$return = 'No se ha podido añadir el usuario a la BD.';
							return $return;
						}
					}
					else {
						// Manda mensaje de error y regresa a contactmaint
						$return = 'No se ha añadido el usuario. Falta información obligatoria.';
						return $return;
					}
				}
		
			// Devuelve falso si ha habido error
			protected function _verifyInput() {
				$error = false;
					if (!trim($this->name)) {
						$error = true;
					}
					if (!trim($this->pass)) {
						$error = true;
					}
					return !$error;
				}

			}
			
?>