
<?php 
	include_once 'includes/init.php';
	$token = createToken();
	$_SESSION['token'] = $token;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s7</title>
	<link rel="stylesheet" href="css/style.css">
	<script src="js/function.js"></script>
</head>
	<body>
		<h1>CRUD de Usuarios</h1>
		<?php $users = User::getUsers();?>
		<table>
			<thead>
				<tr>
					<th>Id: </th>
					<th>Name: </th>
					<th>Password: </th>
					<th>Actions: </th>
				</tr>
			</thead>
			<tbody id="table-body">
				<?php foreach($users as $user) :  ?>
					<tr id="<?= $user->getId() ?>">
						<td><?= $user->getId(); ?></td>
						<td><?= $user->getName(); ?></td>
						<td><?= $user->getPass(); ?></td>
						<td>	
							<button data-id="<?= $user->getId()?>"
									data-name="<?= $user->getName()?>"
									data-pass="<?=$user->getPass()?>"
							>
								Ver
							</button>							
							<button data-id="<?= $user->getId()?>">
								Borrar
							</button>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>


		<!-- The Modal -->
		<div id="user-modal" class="modal">

			<!-- Modal content -->
			<div class="modal-content">
				<span id="user-close-modal" class="close">&times;</span>
				<form name="updateForm">
					<label for="name">Name:</label>
					<br/>
					<input type="text" id="name" name="name" 
					value=""/>
					<br/>
					<label for="pass">Pass</label><br />
					<input type="text" name="pass"  value=""/>
					<br/>
					<input type="hidden" name="id"  value="" />
					<input type="hidden" name="token"  value="<?= $token ?>" />
					<button type="submit">Actualizar</button>
				</form>
			</div>
		</div>

		<div id="snackbar"></div>
	</body>

</html>