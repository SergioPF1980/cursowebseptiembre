<?php
	include_once 'includes/init.php';
?>
<?php
	if (!checkToken($_POST['token']))	{
		die('token incorrecto');
	}
	foreach ($_POST as $key => $value) {
		$_POST[$key] = filter_var($_POST[$key], FILTER_SANITIZE_STRING);
	}
	// unset($_SESSION['token']);
	
	try {
		$user = new User($_POST);
		$response = $user->updateUser();
		echo $response;
	} catch (Exception $error) {
		http_response_code(500);
		echo 'Excepción: ',  $error->getMessage(), "\n";
	}
	// registerUser($_POST);




	function registerUser($params) {
		$newUser = new User($params);
		echo $newUser->addRecord();
	}
?>