window.addEventListener('load', init);

function init() {
    updateForm.addEventListener("submit", updateUser);
    var tBody = document.getElementById("table-body");
    tBody.addEventListener("click", (clickEvent) => {
        if (clickEvent.target.nodeName == 'BUTTON') {
            var modal = document.getElementById("user-modal");
            modal.style.display = "block";

            let id = clickEvent.target.getAttribute("data-id");
            let name = clickEvent.target.getAttribute("data-name");
            let pass = clickEvent.target.getAttribute("data-pass");
            
            updateForm.name.value = name;
            updateForm.id.value = id;
            updateForm.pass.value = pass; 
        }

        var spanClose = document.getElementById("user-close-modal");
        spanClose.addEventListener("click", () => {
            modal.style.display = "none";
        });
    });
}

function updateUser(evt){
    evt.preventDefault();
    var name = updateForm.name.value;
    var pass = updateForm.pass.value;
    var id = updateForm.id.value;
    var token = updateForm.token.value;

    var formData = new FormData();
    formData.append('name', name);
    formData.append('pass', pass);
    formData.append('id', id);
    formData.append('token', token);

    fetch('userRequest.php', {
        method: 'POST',
        body: formData
    }).then(response => {
        if(response.ok){
            return response.text();
        }else {
            throw new Error('Petición fallida');
        }
    })
    .then(message => {
        showSnackBar(message);
    }).catch(error => {
        showSnackBar(error.message, true);
    });
}

function showSnackBar(message, isError = false) {
    var snackbar = document.getElementById("snackbar");
    snackbar.innerHTML = message;
    snackbar.className = "show";
    if(isError) {
        snackbar.classList.add('error');
    }
    setTimeout(() => { 
        snackbar.className = snackbar.className.replace("show", ""); 
    }, 3000);
}