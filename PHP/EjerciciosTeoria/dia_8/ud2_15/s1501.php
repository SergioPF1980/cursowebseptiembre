<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s15</title>
</head>
	<body>
	<h1>Probando conexión BD</h1>
	<?php
		/*
			ZONA DE DEFINCIÓN PARÁMETROS DE ACCESO A LA BASE DE DATOS
		*/
		define("HOSTNAME", "localhost");
		define("MYSQLUSER", "mtw");
		define("MYSQLPWD", "localhost");
		define("MYSQLDB", "bd_mtw");
	?>
	<?php
		/*
			ZONA DE UTILIZACIÓN DE LA DEFINICIÓN DEL ACCESO A LA BASE DE DATOS
		*/

		$con = new mysqli(HOSTNAME, MYSQLUSER, MYSQLPWD, MYSQLDB);
		if ($con->connect_error) {
			die('Error de conexión: ' . $con->connect_error); 
		}
		else {
			echo '<p>Conexión realizada a MySQL</p';
		}



		$query=$query="select * from contacts";
		
		if(!$result=$con->query($query)){
			echo "Error en la consulta: ".$con->error;
		}
		else{
			print_r($result);
			echo "<br/>";
			while($r = $result->fetch_array(MYSQLI_ASSOC)) {
				//print_r($r); echo '<br />';
				foreach($r as $clave => $valor){
					echo "$clave : $valor <br />";
				}
				echo '<br />';
			}
		}
	?>
	</body>
</html>
