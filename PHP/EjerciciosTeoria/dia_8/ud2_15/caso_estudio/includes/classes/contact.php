<?php
			
	class contact{
			 	// Propiedades protegidas
			 	//No accesibles desde fuera, sólo por padre o hijos
				protected $first_name;
				protected $last_name;
				protected $position;
				protected $email;
				protected $phone;
				protected $id;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				//Métodos
				public function name() //Devuelve el nombre completo del contacto
				{
					echo $this->first_name." ".$this->last_name;
				}		

				//Los siguientes permiten un acceso controlado a las propiedades, 
				//no de forma directa como antes
				public function getFirst_name(){
					return $this->first_name;
				}
				
				public function getLast_name(){
					return $this->last_name;
				}
				
				public function getPosition(){
					return $this->position;
				}
				
				public function getEmail(){
					return $this->email;
				}
				
				public function getPhone(){
					return $this->phone;
				}		

				public function getId(){
					return $this->id;
				}
				public function addRecord() {
					// Verifica los campos
					if ($this->_verifyInput()) {
						$connection = Database::getConnection();
						// Prepara la información
						$query = "INSERT INTO contacts(first_name, last_name, position, email, phone)
						VALUES ('" . Database::prep($this->first_name) . "', '" . Database::prep($this->last_name) . "',
						'" . Database::prep($this->position) . "',
						'" . Database::prep($this->email) . "',
						'" . Database::prep($this->phone) . "')";
						// Ejecuta la sentencia MySql
						if ($connection->query($query)) {
							$return = array('', 'Contacto añadido correctamente');
							return $return;
						}
						else {
							// Manda mensaje de error y regresa a contactmaint
							$return = array('contactmaint', 'No se ha añadido el contacto. No se ha podido añadir a la BD.');
							return $return;
						}
					}
					else {
						// Manda mensaje de error y regresa a contactmaint
						$return = array('contactmaint', 'No se ha añadido el contacto. Falta información obligatoria.');
						return $return;
					}
				}
		
			// Devuelve falso si ha habido error
			protected function _verifyInput() {
				$error = false;
					if (!trim($this->first_name)) {
						$error = true;
					}
					if (!trim($this->last_name)) {
						$error = true;
					}
					return !$error;
			}

	}
			
?>