<?php
			
	function loadContent($where, $default=''){
		//El valor por defecto de content es home
			//$content='home';//Esto ya no tienes sentido

			$content=filter_input(INPUT_GET,$where, FILTER_SANITIZE_STRING);//Filtramos la entrada que proviene de un GET
			$default=filter_var($default, FILTER_SANITIZE_STRING);
			$content=(empty($content)) ? $default : $content;

			/*El siguiente código no es necesario porque la comprobación ya se ha hecho en el filtrado
			//Obtenemos el valor de content si ha sido enviada con GET
			if(isset($_GET['content']))
			{
				$content=$_GET['content'];
			}

			//Pasamos filtro por seguridad
			$content = filter_var($content, FILTER_SANITIZE_STRING);	

			*/

			//Incluimos el contenido en base al valor de content
			if($content)//Se pone porque en el caso de las página que no tiene menú lateral ... daría error porque $content está vacío en la llamada ... no tiene valor al no tener menú, sidebar está vacía
			{
		 		$html=include 'content/'.$content.'.php';
		 		return $html;
		 	}
	}
		
	// Mantenimiento de contactos
	function maintContact() {
		$results = '';
		// Se ha pulsado el botón save
		if (isset($_POST['save']) AND $_POST['save'] == 'Save') {
			// Comprueba el token
			$badToken = true;
			if (!isset($_POST['token'])
			|| !isset($_SESSION['token'])
			|| empty($_POST['token'])
			|| $_POST['token'] !== $_SESSION['token']) {
				$results = array('','Hay un problema de seguridad');
				$badToken = true;
			}
			else {
				$badToken = false;
				unset($_SESSION['token']);
				// Pone las variables "limpias" en un vector
				$item = array ( 'id' => (int) $_POST['id'],
				'first_name' => filter_input(INPUT_POST,'first_name', FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES),
				'last_name' => filter_input(INPUT_POST,'last_name', FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES),
				'position' => filter_input(INPUT_POST,'position', FILTER_SANITIZE_STRING,FILTER_FLAG_NO_ENCODE_QUOTES),
				'email' => filter_input(INPUT_POST,'email', FILTER_SANITIZE_STRING),
				'phone' => filter_input(INPUT_POST,'phone', FILTER_SANITIZE_STRING)
				);
				// Creamos un nuevo contacto basado en la información POST
				$contact = new Contact($item);
				$results = $contact->addRecord();
			}
		}
		return $results;
	}		
?>