<?php session_start(); // comienza o retoma una sesión



	//Indicamos que se han de mostrar todo tipo de errores en tiempo de ejecución
	error_reporting(E_ALL);//Así se indica qué errores mostrar; en nuestro caso todos 
	ini_set('display_errors', 'On'); //Comenzamos a mostrar errores, lo activamos


	//Inserción de funciones
	require_once 'includes/functions.php';
	

	//Carga automática de clases cuando sea necesario, es decir, en su primer uso
	function _autoload($clase) 
	{
		require_once 'includes/classes/'.strtolower($clase) . '.php';
	}



	// Inicializa mensaje entrante
	$message = '';
	if (isset($_SESSION['message'])) {
		$message = htmlentities($_SESSION['message']);
		unset($_SESSION['message']);
	}

	// Proceso basado en la tarea, por defecto visualizar
	$task = filter_input(INPUT_POST,'task', FILTER_SANITIZE_STRING);
	switch ($task) {
		case 'contact.maint' :
			// Mantenimiento
			$results = maintContact();
			$message .= $results[1];
			// Si hay redirección va a la página
			if ($results[0] == 'contactmaint') {
				// Redirige a una página nueva
				if ($results[1]) {
					$_SESSION['message'] = $results[1];
				}
				header("Location: index.php?content=contactmaint");
				exit;
			}
			break;
	}

?>