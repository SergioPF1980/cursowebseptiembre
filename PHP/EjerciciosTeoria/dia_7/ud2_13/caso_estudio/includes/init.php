<?php 

	//Indicamos que se han de mostrar todo tipo de errores en tiempo de ejecución
	error_reporting(E_ALL);//Así se indica qué errores mostrar; en nuestro caso todos 
	ini_set('display_errors', 'On'); //Comenzamos a mostrar errores, lo activamos


	//Inserción de funciones
	require_once 'includes/functions.php';
	

	//Carga automática de clases cuando sea necesario, es decir, en su primer uso
	/*function __autoload($clase) 
	{
		require_once 'includes/classes/'.strtolower($clase) . '.php';
	}*/
	function custom_autoload( $class ) {    
		require_once 'includes/classes/'.strtolower($clase) . '.php';
	}
	spl_autoload_register('custom_autoload');
	
?>