<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s13</title>
</head>
	<body>
	<h1>Probando conexión BD</h1>
	<?php
		/*
			ZONA DE DEFINCIÓN PARÁMETROS DE ACCESO A LA BASE DE DATOS
		*/
		define("HOSTNAME", "localhost");
		define("MYSQLUSER", "mtw");
		define("MYSQLPWD", "localhost");
		define("MYSQLDB", "bd_mtw");
	?>
	<?php
		/*
			ZONA DE UTILIZACIÓN DE LA DEFINICIÓN DEL ACCESO A LA BASE DE DATOS
		*/

		$con = new mysqli(HOSTNAME, MYSQLUSER, MYSQLPWD, MYSQLDB);
		if ($con->connect_error) {
			die('Error de conexión: ' . $con->connect_error); 
		}
		else {
			echo 'Conexión realizada a MySQL';
		}
	?>
	
	</body>
</html>
