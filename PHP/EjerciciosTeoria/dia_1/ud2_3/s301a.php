<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s3 - COMENTARIOS</title>
</head>
	<body>
		// Comentario de una sola línea
		<h1>Encabezado</h1>
		/* Comentario en bloque
		comprende todo lo situado en el bloque
		que es delimitado por las marcas correspondientes
		*/
		<p>Cuerpo</p> 
		
	</body>
</html>

