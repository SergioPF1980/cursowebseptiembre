<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s4</title>
</head>
	<body>
	
	<?php
		echo date('l, F j, Y', strtotime('12/5/2011')) . '<br />';
		echo date('l, F j, Y', strtotime('yesterday', strtotime('12/5/2011')))
		. '<br />';
		echo date('l, F j, Y', strtotime('yesterday')) . '<br />';
		echo date('l, F j, Y', strtotime('now')) . '<br />';
		echo date('l, F j, Y', strtotime('Dec 5 2011')) . '<br />';
		echo date('l, F j, Y', strtotime('+4 hours')) . '<br />';
		echo date('l, F j, Y', strtotime('+1 week')) . '<br />';
		echo date('l, F j, Y', strtotime('+2 weeks 1 day 4 hours 10 seconds'))	. '<br />';
		echo date('l, F j, Y', strtotime('next Tuesday')) . '<br />';
		echo date('l, F j, Y', strtotime('last Monday'));
		
		
	?>
	
	</body>

</html>
