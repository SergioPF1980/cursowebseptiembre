		<?php
		//Zona con la información sobre cada uno de los lotes existentes
		$lot_number1 = '1';
		$name1 = "Naval Officer’s Formal Tailcoat, 1840s";
		$description1 = 'Black wool broadcloth, double breast front, missing
		3 of 18 raised round gold buttons w/crossed cannon barrels & "Ordnance Corps" text, silver sequin & tinsel embroidered emblem on each square cut tail, quilted black silk lining, very good; ';
		$price1 = 5700.00;
		
		$lot_number2 = '2';
		$name2 = "Striped Cotton Tailcoat, America, 1835-1845";
		$description2 = 'Orange and white pin-striped twill cotton, double breasted, turn down collar, waist seam, 
					self-fabric buttons, inside single button pockets in each tail, (soiled, faded, cuff edges
					 frayed) good. ';
		$price2 = 20700.00;
		
		$lot_number3 = '3';
		$name3 = "Black Broadcloth Tailcoat, 1830-1845";
		$description3 = 'Fine thin wool broadcloth, double breasted, notched collar, horizontal front and side waist seam,
					 slim long sleeves with notched cuffs, curved tails, black silk satin lining quilted in diamond pattern,
					  padded and quilted chest, black silk covered buttons, (buttons worn) excellent.';
		$price3 = 3450.00;
		

		//Variable para el control del formato de filas pares e impares
		$i=0;

		?>

		<h1>Product Category: Gents</h1>

		<ul class="ulfancy">
			
			<li class="row<?php echo $i % 2; ?>">							
				<div class="list-description">
					<h2><?php echo $name1 ?></h2>
					<p><?php echo $description1 ?></p>
					<p><strong>Lot:</strong> #<?php echo $lot_number1 ?> 
					<strong>Price:</strong> $<?php echo number_format($price1,2) ?></p>
				</div>			
				<div class="clearfloat"></div>
			</li>
			
			
			<?php $i++; ?>
			<li class="row<?php echo $i % 2; ?>">
				<div class="list-description">
					<h2><?php echo $name2 ?></h2>
					<p><?php echo $description2 ?></p>
					<p><strong>Lot:</strong> #<?php echo $lot_number2 ?> 
					<strong>Price:</strong> $<?php echo number_format($price2,2) ?></p>
				</div>
				<div class="clearfloat"></div>
			</li>
			
			<?php $i++; ?>
			<li class="row<?php echo $i % 2; ?>">			
				<div class="list-description">
					<h2><?php echo $name3 ?></h2>
					<p><?php echo $description3 ?></p>
					<p><strong>Lot:</strong> #<?php echo $lot_number3 ?> 
					<strong>Price:</strong> $<?php echo number_format($price3,2) ?></p>
				</div>
				<div class="clearfloat"></div>
			</li>
	
		</ul>
