<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s4</title>
</head>
	<body>
	
	<?php
		$var1 = TRUE;
		$var2 = 'FALSE';

		echo "<p>";
		if($var1) 
			echo "True 1";
		else
			echo "False 1";
		echo "</p>";

		echo "<p>";
		if($var2) 
			echo "True 2";
		else
			echo "False 2";
		echo "</p>";
	?>
	
	</body>

</html>
