<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s4</title>
</head>
	<body>
	
	<?php
		$usuario1 = 'Pepe López';
		$usuario2 = 'Juan Pérez';
		$usuario3 = 'Cristina Sánchez';
	?>
	
	<h1>Lista de usuarios</h1>
	
	<!--Mostramos una a una las variables individuales -->
	<p><?php echo $usuario1; ?></p>
	<p><?php echo $usuario2; ?></p>
	<p><?php echo $usuario3; ?></p>
	
	<!--Primer ejemplo con array -->
	<?php
		$usuarios = array('Pepe López', 'Juan Pérez', 'Cristina Sánchez');
	?>
	<pre>
	<p><?php number_format($usuarios); ?></p>
	</pre>
	<!--Segundo ejemplo con array -->
	<?php
		$elementos[0] = 'elemento1';
		$elementos[1] = 'elemento2';
		$elementos[2] = 'elemento3';
		echo "<p> $elementos[1]</p>";
	?>

	<!--Tercer ejemplo con array -->
	<?php
		$datos = array('nombre' => 'Pepe López', 'edad' => 27, 'despacho' =>'2c1');
		print_r($datos);
		echo "<p> $datos[edad]</p>";
	?>

	<!--Cuarto ejemplo con array -->
	<?php
		$empleado1 = array('nombre' => 'Pepe López', 'edad' => 27, 'despacho' =>'2c1');
		$empleado2 = array('nombre' => 'Charo Seint', 'edad' => 22, 'despacho' =>'4b6');
		$empleado3 = array('nombre' => 'Juan Soler', 'edad' => 21, 'despacho' =>'4b5');
		
		$empleados = array($empleado1, $empleado2, $empleado3);

		/*Segunda alternativa para definir empleados
			$empleados = array(
			array('nombre'=>'Pepe López', 'edad'=>27, 'despacho'=>'2c1' ),
			array('nombre'=>'Charo Seint', 'edad'=>22, 'despacho'=>'4b6' ),
			array('nombre'=>'Juan Soler', 'edad'=>21, 'despacho'=>'4b5' ),
			);
		*/
		echo "<pre>";
		print_r($empleados);
		echo "</pre>";
	?>


	</body>

</html>
