<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s4</title>
</head>
	<body>
	
	<?php
		define('DATABASE','mydatabase');	
		define('USERNAME','admin');
		define('PASSWORD','s0mePassw0rd');
	?>

	<p>
		This program uses the <?php echo DATABASE; ?> database with the user name
		<?php echo USERNAME; ?> and password <?php echo PASSWORD ?>.
	</p>
	
	
	<?php
		if(defined('DATABASE'))
			echo "Está definida y su valor es " . DATABASE;
		else
			echo "No está definida";
	?>
	
	</body>

</html>
