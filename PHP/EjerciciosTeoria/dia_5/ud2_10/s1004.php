<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s10</title>
</head>
	<body>
		<?php
			
			function sumar($a, $b){
				if(is_numeric($a) and is_numeric($b))
					return $a+$b;
				else
					return false;
			}
			
		?>
		
		<?php
			//$resultado=sumar(2,3);
			$resultado=sumar(2,"veinte");
			echo ($resultado) ? $resultado : "Parámetros incorrectos";
		?>
	
	</body>
</html>
