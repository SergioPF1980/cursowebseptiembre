<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s10</title>
</head>
	<body>
		<?php
			
			function cuenta($i){
				echo ++$i;//Ahora no es necesario asignar valor a i porque al pasarla como parámetro ya cuenta su definición en el for
			}
			
			function cuentaRF(&$i){
				echo ++$i;
			}
		?>
		
		<?php
			for($i=0;$i<5;$i++){
				cuenta($i);
				//cuentaRF($i);
			}
		?>
	
	</body>
</html>
