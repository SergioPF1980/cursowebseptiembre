<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MTW -PHP-</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />

<?php require_once 'includes/functions.php'; ?>

</head>

<body>
<div id="container">

	<div id="header">
		<a href="index.php">
			<img src="images/banner.jpg"  alt="Pick some ideas that work" />
		</a> 
	</div><!-- end header -->
	
	<div id="navigation">
		<h3 class="element-invisible">Menu</h3>
		<ul class="mainnav"><!-- Jugamos con el link para dar valor a la variable content que me permite
			saber cuándo poner el menú común de categories, about y home-->
        	<li><a href="index.php?content=categories">Lot Categories</a></li>
        	<li><a href="index.php?content=about">About Us</a></li>
        	<li><a href="index.php?content=home">Home</a></li>
		</ul>
		<div class="clearfloat"></div>
	</div><!-- end navigation -->

	<div class="message">
	</div><!-- end message -->	
	
	<!-- Se incluye para cada caso adecuado, la nevagación existente en catnav.php, es decir, para
	categories, about y home -->
	<div class="sidebar">
		<?php
			loadContent('sidebar', '');//content siempre tendrá un valor que será la sección, menos en la primera carga de la web
		 ?>
	</div><!-- end sidebar -->
	
	<div class="content">
		<?php
			loadContent('content', 'home');//content siempre tendrá un valor que será la sección, menos en la primera carga de la web
		 ?>
	</div><!-- end content -->
	
	<div class="clearfloat"></div>
	
	<div id="footer">
		<p>Copyleft</p>
	</div><!-- end footer -->

</div><!-- end container -->
</body>
</html>
