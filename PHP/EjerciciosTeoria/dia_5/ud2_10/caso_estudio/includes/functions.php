<?php
			
	function loadContent($where, $default=''){
		//El valor por defecto de content es home
			//$content='home';//Esto ya no tienes sentido

			$content=filter_input(INPUT_GET,$where, FILTER_SANITIZE_STRING);//Filtramos la entrada que proviene de un GET
			$default=filter_var($default, FILTER_SANITIZE_STRING);
			$content=(empty($content)) ? $default : $content;

			/*El siguiente código no es necesario porque la comprobación ya se ha hecho en el filtrado
			//Obtenemos el valor de content si ha sido enviada con GET
			if(isset($_GET['content']))
			{
				$content=$_GET['content'];
			}

			//Pasamos filtro por seguridad
			$content = filter_var($content, FILTER_SANITIZE_STRING);	

			*/

			//Incluimos el contenido en base al valor de content
			if($content)//Se pone porque en el caso de las página que no tiene menú lateral ... daría error porque $content está vacío en la llamada ... no tiene valor al no tener menú, sidebar está vacía
			{
				$html=include 'content/'.$content.'.php';
				return $html;
			}
	}
			
?>