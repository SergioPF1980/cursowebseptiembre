<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s10</title>
</head>
	<body>
		<?php	

			function cuenta1()
			{
				$i=0;/*Esto no está en las transparencia!!!!!!! OJO !!!!!!
					 Da error si no la defino porque no la paso como parámetro, por lo que su
					 definición en el for no cuenta
					*/
				echo ++$i;
			}

			function cuenta2()
			{
				static $i;
				echo ++$i;
			}
		?>
		
		<?php	
			for($i=0;$i<5;$i++)
			{
				cuenta1();
				// cuenta2();
			}
		?>
	</body>
</html>
