<?php
			setcookie("nombre",filter_var($_POST["username"], FILTER_SANITIZE_STRING), time()+(60*60*24)); 
			setcookie("pass",filter_var($_POST["password"], FILTER_SANITIZE_STRING), time()+(60*60*24));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s8</title>
</head>
	<body>
		<p>Has introducido <?php echo filter_var($_POST["username"], FILTER_SANITIZE_STRING) ?> como nombre de usuario
		y <?php echo filter_var($_POST["password"], FILTER_SANITIZE_STRING) ?> como contraseña.</p>
		<a href="s804a.php">Volver</a>
	</body>
</html>
