<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s8</title>
</head>
	<body>
		<?php
			$weather='sunny';
			if($weather=='rainy')
			{
				echo "<p>It will be rainy today. User your umbrella.</p>";
			}
			elseif ($weather=='sunny') {
				echo "<p>It will be sunny today. Wear your sunglasses.</p>";
			}
			elseif ($weather=='snowy') {
				echo "<p>It will be snowy today. Bring your shovel.</p>";
			}
			else
			{
				echo "<p>I don't know what the weather is doing today..</p>";
			}

		?>
	</body>
</html>
