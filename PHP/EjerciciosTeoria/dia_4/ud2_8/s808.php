<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s8</title>
</head>
	<body>
		<?php
			$state = 'MA';
			switch ($state) { 
				case 'ME':
				case 'VT':
				case 'NH':
					echo "<p>Northern New England</p>";
					break;
				case 'CT':
				case 'MA':
				case 'RI':
					echo "<p>Southern New England</p>";
					break;
				default:
					echo "<p>$state is not in New England.</p>";
			}
		?>
	
	</body>
</html>
