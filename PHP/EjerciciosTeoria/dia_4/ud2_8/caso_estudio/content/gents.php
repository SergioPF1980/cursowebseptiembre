		<?php
		//Zona con la información sobre cada uno de los lotes existentes
		//Definimos un array con la información de cada uno de los lotes
		//Es un array bidimensional donde la primera dimensión hace referencia a cada lote que es representado
		//a su vez por un array con los diferentes elementos que lo describen y componen
		$lots[0]['lot_number'] = '1';
		$lots[0]['lot_name']= "Naval Officer’s Formal Tailcoat, 1840s";
		$lots[0]['description']='Black wool broadcloth, double breast front, missing
		3 of 18 raised round gold buttons w/crossed cannon barrels & "Ordnance Corps" text, silver sequin & tinsel embroidered emblem on each square cut tail, quilted black silk lining, very good; ';
		$lots[0]['price'] = 5700.00;
		
		$lots[1]['lot_number'] = '2';
		$lots[1]['lot_name'] = "Striped Cotton Tailcoat, America, 1835-1845";
		$lots[1]['description'] = 'Orange and white pin-striped twill cotton, double breasted, turn down collar, waist seam, 
					self-fabric buttons, inside single button pockets in each tail, (soiled, faded, cuff edges
					 frayed) good. ';
		$lots[1]['price'] = 20700.00;
		
		$lots[2]['lot_number'] = '3';
		$lots[2]['lot_name'] = "Black Broadcloth Tailcoat, 1830-1845";
		$lots[2]['description'] = 'Fine thin wool broadcloth, double breasted, notched collar, horizontal front and side waist seam,
					 slim long sleeves with notched cuffs, curved tails, black silk satin lining quilted in diamond pattern,
					  padded and quilted chest, black silk covered buttons, (buttons worn) excellent.';
		$lots[2]['price'] = 3450.00;
		

		//Variable para el control del formato de filas pares e impares
		$i=0;

		?>

		<h1>Product Category: Gents</h1>

		<ul class="ulfancy">
			
			<li class="row<?php echo $i % 2; ?>">							
				<div class="list-description">
					<h2><?php echo $lots[$i]['lot_name'] ?></h2>
					<p><?php echo $lots[$i]['description'] ?></p>
					<p><strong>Lot:</strong> #<?php echo $lots[$i]['lot_number'] ?> 
					<strong>Price:</strong> $<?php echo number_format($lots[$i]['price'],2) ?></p>
				</div>			
				<div class="clearfloat"></div>
			</li>
			
			
			<?php $i++; ?>
			<li class="row<?php echo $i % 2; ?>">
				<div class="list-description">
					<h2><?php echo $lots[$i]['lot_name'] ?></h2>
					<p><?php echo $lots[$i]['description'] ?></p>
					<p><strong>Lot:</strong> #<?php echo $lots[$i]['lot_number'] ?> 
					<strong>Price:</strong> $<?php echo number_format($lots[$i]['price'],2) ?></p>
				</div>
				<div class="clearfloat"></div>
			</li>
			
			<?php $i++; ?>
			<li class="row<?php echo $i % 2; ?>">			
				<div class="list-description">
					<h2><?php echo $lots[$i]['lot_name'] ?></h2>
					<p><?php echo $lots[$i]['description'] ?></p>
					<p><strong>Lot:</strong> #<?php echo $lots[$i]['lot_number'] ?> 
					<strong>Price:</strong> $<?php echo number_format($lots[$i]['price'],2) ?></p>
				</div>
				<div class="clearfloat"></div>
			</li>
	
		</ul>
