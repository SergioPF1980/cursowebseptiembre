<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s9</title>
</head>
	<body>
	<?php
		$empleados=array(
			array('nombre'=>'Pepe López','edad'=>27,'despacho'=>'2c1'),
			array('nombre'=>'Charo Seint','edad'=>22,'despacho'=>'4b6'),
			array('nombre'=>'Juan Soler','edad'=>21,'despacho'=>'4b5'));
	?>
	<ul>
		<?php foreach($empleados as $empleado): ?>
		<li>
		<?php echo $empleado['nombre']; ?>	
		</br>
		<?php echo $empleado['edad']; ?>
		</br>
		<?php echo $empleado['despacho']; ?>
		</li>
		<?php endforeach; ?>
	</ul>

	</body>
</html>
