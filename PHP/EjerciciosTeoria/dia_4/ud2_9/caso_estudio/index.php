<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MTW -PHP-</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">

	<div id="header">
		<a href="index.php">
			<img src="images/banner.jpg"  alt="Pick some ideas that work" />
		</a> 
	</div><!-- end header -->
	
	<div id="navigation">
		<h3 class="element-invisible">Menu</h3>
		<ul class="mainnav"><!-- Jugamos con el link para dar valor a la variable content que me permite
			saber cuándo poner el menú común de categories, about y home-->
        	<li><a href="index.php?content=categories">Lot Categories</a></li>
        	<li><a href="index.php?content=about">About Us</a></li>
        	<li><a href="index.php?content=home">Home</a></li>
		</ul>
		<div class="clearfloat"></div>
	</div><!-- end navigation -->

	<div class="message">
	</div><!-- end message -->	
	
	<!-- Se incluye para cada caso adecuado, la nevagación existente en catnav.php, es decir, para
	categories, about y home -->
	<div class="sidebar">
		<?php
			if(isset($_GET['content']))//Si no pongo esto me da error porque no existe valor alguno para content de manera inicial
			{
				switch ($_GET["content"]) :
					case 'gents':
					case 'sporting':
					case 'woman':
						include 'content/catnav.php';
				endswitch;
			}
		?>	
	</div><!-- end sidebar -->
	
	<div class="content">
		<?php
			//El valor por defecto de content es home
			$content='home';

			//Obtenemos el valor de content si ha sido enviada con GET
			if(isset($_GET['content']))
			{
				$content=$_GET['content'];
			}

			//Pasamos filtro por seguridad
			$content = filter_var($content, FILTER_SANITIZE_STRING);	

			//Incluimos el contenido en base al valor de content
		 	include 'content/'.$content.'.php';
		 ?>
	</div><!-- end content -->
	
	<div class="clearfloat"></div>
	
	<div id="footer">
		<p>Copyleft</p>
	</div><!-- end footer -->

</div><!-- end container -->
</body>
</html>
