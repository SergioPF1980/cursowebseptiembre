<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s11</title>
</head>
	<body>
		<?php
	
		class miClase {
			// propiedades
			public $pro1;
			public $pro2;
			public $pro3;
			// Métodos
			// Constructor
			public function __construct($entrada=false){
				if(is_array($entrada)){
					foreach($entrada as $clave=>$valor){
						$this->$clave=$valor;/*Así se define un nuevo elemento en forma de dupla clave(índice)=>valor
											 por cada dupla del parámetro
											 Queda definido como elemento de la clase aunque fuera no esté definido */
					}
				}
			}
			
			public function test($entrada=false){
				$res='';
				if(is_array($entrada)){
					foreach($entrada as $valor){
						$res = $res.' - '.$this->$valor;
					}
				}
				return $res;
			}
		}
					
	?>
	
	<?php
		$v1=new miClase(array(pro1=>'v1'));
		echo $v1->test(array('pro1'));
		echo '<br />';
		$v2=new miClase(array(pro1=>'p1',pro2=>'p2',pro3=>'p3'));
		echo $v2->test(array('pro2','pro3'));
	?>
	</body>
</html>
