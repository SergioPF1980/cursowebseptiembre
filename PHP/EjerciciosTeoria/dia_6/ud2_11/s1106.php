<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s11</title>
</head>
	<body>
		<?php	

		//DEFINICIÓN DE LA CLASE PUNTO

			 class punto
			 {
			 	//Propiedades
			 	public $X="_";
			 	public $Y="_";
			 	public $nombre="_";
			 	
			 	//Constructor
				public function __construct($newX=false,$newY=false,$newNombre=false)
				{
					if(is_integer($newX))
			 		{
			 			$this->X=$newX;
			 		}

			 		if(is_integer($newY))
			 		{
			 			$this->Y=$newY;
			 		}

			 		if(is_string($newNombre))
			 		{
			 			$this->nombre=$newNombre;
			 		}
				}
			
			/*OTRA MANERA DE HACERLO
			
			// Propiedades
			public $x;
			public $y;
			public $etiqueta;
			// Métodos
			// Constructor
			public function __construct($x="_",$y="_",$etiqueta="_"){
				$this->x=$x;
				$this->y=$y;
				$this->etiqueta=$etiqueta;
			}

			*/

			 	//Métodos
			 	public function getCoordenadas()
			 	{
			 		return 	"(".$this->X.",".$this->Y.")";		 	
			 	}
			 	
			 	public function getInformacion()
			 	{
			 		return 	$this->nombre." (".$this->X.",".$this->Y.")";

			 	}

			 	public function setX($newX)
			 	{
			 		if(is_integer($newX))
			 		{
			 			$this->X=$newX;
			 		}
			 	}

			 	public function setY($newY)
			 	{
			 		if(is_integer($newY))
			 		{
			 			$this->Y=$newY;
			 		}
			 	}

			 	public function setEtiqueta($newEtiqueta)
			 	{
			 		if(is_string($newEtiqueta))
			 		{
			 			$this->nombre=$newEtiqueta;
			 		}
			 	}
			 }
		?>
		
		<?php	
		
		//DEFINICIÓN DE LA CLASE PIXEL

			class pixel extends punto
			{
				public $R=0;
				public $G=0;
				public $B=0;
				
				function __construct($newX,$newY,$newNombre,$newR,$newG,$newB)
				{
					parent::__construct($newX,$newY,$newNombre);
					$this->R=$newR;
					$this->G=$newG;
					$this->B=$newB;
				}

				public function getInformacion()
			 	{
			 		return parent::getInformacion(). " [ ".$this->R.",".$this->G.",".$this->B." ]";
			 	}
			}

		?>
		
		<?php	
			$p2=new punto(7,8,"MiPunto2");
			echo  $p2->getInformacion();
			echo '<br />';
			$px1=new pixel(7,8,"px1",0.2,0.1,0.9);
			echo  $px1->getInformacion();

		?>
	</body>
</html>
