<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s11</title>
</head>
	<body>
	
	<?php
	
		abstract class miClaseBase {
			abstract protected function getItem();
			abstract protected function quantity($qty);
			public function listItem() {
				$result = '<p>' . $this->getItem() . '</p>';
				return $result;
			}
		}
	
	
		class miClaseHija extends miClaseBase {
			protected function getItem() {
				return "This is an Item";
			}
			public function quantity($qty) {
				return '<p>Your quantity is ' . $qty . '.</p>';
			}
		}
	
	?>
	</body>
</html>
