<?php 

	require_once 'includes/functions.php';
	

	//Carga automática de clases cuando sea necesario, es decir, en su primer uso
	function __autoload($clase) 
	{
		require_once 'includes/classes/'.strtolower($clase) . '.php';
	}
?>