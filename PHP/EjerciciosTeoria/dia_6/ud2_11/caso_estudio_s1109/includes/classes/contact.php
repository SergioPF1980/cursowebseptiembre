<?php
			
	class contact
			 {
			 	// Propiedades protegidas
			 	//No accesibles desde fuera, sólo por padre o hijos
				protected $first_name;
				protected $last_name;
				protected $position;
				protected $email;
				protected $phone;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				//Métodos
				public function name() //Devuelve el nombre completo del contacto
				{
					echo $this->first_name." ".$this->last_name;
				}		

				//Los siguientes permiten un acceso controlado a las propiedades, 
				//no de forma directa como antes
				public function getFirst_name(){
					return $this->first_name;
				}
				
				public function getLast_name(){
					return $this->last_name;
				}
				
				public function getPosition(){
					return $this->position;
				}
				
				public function getEmail(){
					return $this->email;
				}
				
				public function getPhone(){
					return $this->phone;
				}		
			}
			
?>