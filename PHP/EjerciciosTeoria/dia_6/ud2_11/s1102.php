<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
	<title>s11</title>
</head>
	<body>
		<?php	
			class miClase
			{
			 	//Propiedades
				public $propiedad1="valor_x_defecto";
				public $propiedad2;
				
			 	//Métodos
				public function action1($p1=false)
				{
					$this->propiedad2=$p1;
			 		//Código PHP
				}

				public function accion2()
				{
			 		//Código PHP
				}
			}
		?>
		<?php	
			nombreFuncion();
		?>
	</body>
</html>
