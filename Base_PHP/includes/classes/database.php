<?php

class Database{
    private static $_hostName = 'localhost';
    private static $_mysqlDB = 'caso_estudio';
    private static $_mysqlUser = 'root';
    private static $_mysqlPass = '';
    
    private static $_connection = null;

    private function __construct($entrada=false) {

    }

    public static function getConnection(){
        if(!self::$_connection){
            self::$_connection = new mysqli(
                                    self::$_hostName, 
                                    self::$_mysqlUser,
                                    self::$_mysqlPass, 
                                    self::$_mysqlDB);

            if(self::$_connection->connect_error){
                die('Error en la base de datos: ' . self::$_connection->connect_error);
            }
        }

        return self::$_connection;
    }

    public static function prep($valor){
        $valor = self::$_connection->real_escape_string($valor);
        return $valor;
    }
}
    


?>