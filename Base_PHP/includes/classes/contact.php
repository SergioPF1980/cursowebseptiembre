<?php 
    class Contact{
        //Propiedades
        public $first_name;
        public $last_name;
        public $position;
        public $email;
        public $phone;

        //Constructor
        public function __construct($entrada = false){
            if(is_array($entrada)){
                foreach($entrada as $clave=>$valor){
                    $this->$clave=$valor;
				}
			}
        }

        //Métodos
		public function getFullName(){
			return $this->first_name . ' '. $this->last_name;
		}

        public function getPosition(){
            return $this->position;
        }

        public function getEmail(){
            return $this->email;
        }

        public function getPhone(){
            return $this->phone;
        }

        public static function getContacts(){
            $items = [];
            $_connection = Database::getConnection();
		    $query = "SELECT * FROM contacts";
				if($result = $_connection->query($query)){

                    while($obj = $result->fetch_object('Contact')) { // pasa un array al constructor
						$items[] = $obj;
					}
                    $result->close();
				}else{
					return false;
				}
                return $items;
        }

        public static function addRegister($name, $last_name, $position, $email, $phone){
            
            $query = "INSERT INTO contacts (first_name, last_name, position, email, phone)
                VALUES ('$name', '$last_name', '$position', '$email', '$phone')";

            $_connection = database::getConnection();
            mysqli_query($_connection, $query);
        }
    }
?>
