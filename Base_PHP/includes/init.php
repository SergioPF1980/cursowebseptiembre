<?php
    //  Muestra los errores de php por pantalla
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

    require_once 'includes/functions.php';

    spl_autoload_register(function ($class) {
        // include 'classes/' .$class . 'contact.php';
        require_once 'includes/classes/' . strtolower($class) . '.php';
    });

?>