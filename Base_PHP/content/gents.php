<h1>Product Category: Gents</h1>

		<ul class="ulfancy">
			
			<li class="row0">							
				<div class="list-description">
					<h2>Naval Officer's Formal Tailcoat, 1840s</h2>
					<p>Black wool broadcloth, double breast front, missing 3 of 18 raised round gold buttons w/ 
					crossed cannon barrels &amp; "Ordnance Corps" text, silver sequin &amp; tinsel embroidered emblem 
					on each square cut tail, quilted black silk lining, very good; </p>
					<p><strong>Lot:</strong> #1 
					<strong>Price:</strong> $5,700.00</p>
				</div>			
				<div class="clearfloat"></div>
			</li>
			
			<li class="row1">
				<div class="list-description">
					<h2>Striped Cotton Tailcoat, America, 1835-1845</h2>
					<p>Orange and white pin-striped twill cotton, double breasted, turn down collar, waist seam, 
					self-fabric buttons, inside single button pockets in each tail, (soiled, faded, cuff edges
					frayed) good. </p>
					<p><strong>Lot:</strong> #2 
					<strong>Price:</strong> $20,700.00</p>
				</div>
				<div class="clearfloat"></div>
			</li>
			
			<li class="row0">			
				<div class="list-description">
					<h2>Black Broadcloth Tailcoat, 1830-1845</h2>
					<p>Fine thin wool broadcloth, double breasted, notched collar, horizontal front and side waist seam,
					slim long sleeves with notched cuffs, curved tails, black silk satin lining quilted in diamond pattern,
					padded and quilted chest, black silk covered buttons, (buttons worn) excellent. </p>
					<p><strong>Lot:</strong> #3 
					<strong>Price:</strong> $3,450.00</p>
				</div>
				<div class="clearfloat"></div>
			</li>
	
		</ul>
