<h1>About Us</h1>
		<p>We are all happy to be a part of this. Please contact any of us with questions.</p>
		
		<ul class="ulfancy">
			<!-- Abrimos el foreach -->
			<?php
			// include ('./includes/classes/contact.php');

			// require_once './includes/classes/contact.php';

			// $newContact1 = new Contact('Martha', 'Smith', 'none', 'martha@example.com', '');
			// $newContact2 = new Contact('George', 'Smith', '', 'george@example.com', '515-555-1236');
			// $newContact3 = new Contact('Jeff', 'Meyers', 'hip hop expert for shure', 'jeff@example.com', '');
			// $newContact4 = new Contact('Peter', 'Meyers', '', 'peter@example.com', ' 515-555-1237');
			// $newContact5 = new Contact('Sally', 'Smith', '', 'sally@example.com', '515-555-1235');
			// $newContact6 = new Contact('Sarah', 'Finder', 'Lost Soul', 'finder@example.com', '555-123-5555');
		
			// $contacts = [$newContact1, $newContact2, $newContact3, $newContact4, $newContact5, $newContact6];
			
			$contacts = [
				$newContact = new Contact('Martha', 'Smith', 'none', 'martha@example.com', ''),
				$newContact = new Contact('George', 'Smith', '', 'george@example.com', '515-555-1236'),
				$newContact = new Contact('Jeff', 'Meyers', 'hip hop expert for shure', 'jeff@example.com', ''),
				$newContact = new Contact('Peter', 'Meyers', '', 'peter@example.com', ' 515-555-1237'),
				$newContact = new Contact('Sally', 'Smith', '', 'sally@example.com', '515-555-1235'),
				$newContact = new Contact('Sarah', 'Finder', 'Lost Soul', 'finder@example.com', '555-123-5555')
			];
			
			$class = 'row0'; 

			foreach($contacts as $user) : ?>
			
			<li class="<?php echo $class ?>">
				
				<h2><?= $user->getFullName(); ?></h2>
				<p>Position: <?= $user->getPosition(); ?><br />
				Email: <?= $user->getEmail(); ?><br />
				Phone: <?= $user->getPhone(); ?> <br /></p>
			</li>

			<?php

			if($class == 'row0'){
				$class = 'row1';
			}else{
				$class ='row0';
			}	

			?>

			<!-- Cerramos el foreach -->
			<?php	
			endforeach
			?>
		</ul>