<?php 

	require_once 'includes/init.php';
	require_once 'includes/functions.php';
	
?>

<?php

?>

<?php
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MTW -PHP-</title>
<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div id="container">

	<div id="header">
		<a href="index.html">
			<img src="images/banner.jpg"  alt="Pick some ideas that work" />
		</a> 
	</div><!-- end header -->
	
	<div id="navigation">
		<h3 class="element-invisible">Menu</h3>
		<ul class="mainnav">
			<li><a href="index.php?content=categories">Lot Categories</a></li>
			<li><a href="index.php?content=about">About Us</a></li>
			<li><a href="index.php?content=home">Home</a></li>
		</ul>
		<div class="clearfloat"></div>
	</div><!-- end navigation -->

	<div class="message">
	</div><!-- end message -->	
	
	<div class="sidebar">
		<?php
		
			// if($_GET['content'] == "category"){
			// 	echo 'Estoy aqui';
			// 	include('./content/gent.php');
			// }
			loadContent('sidebar', '');
		?>
	
	</div><!-- end sidebar -->
	
	<div class="content">
	<?php
		// include('./content/home.php');
		// include('./content/gent.php');
		// include('./content/about.php');
		loadContent('content', 'home');
		// $content='home';

		// if(isset($_GET['content'])){
		// 	$content = $_GET['content'];
		// }

		// $content = filter_var($content, FILTER_SANITIZE_STRING);
		// include 'content/'.$content.'.php';
	?>
	</div><!-- end content -->
	
	<div class="clearfloat"></div>
	
	<div id="footer">
		<p>Copyleft</p>
	</div><!-- end footer -->

</div><!-- end container -->
</body>
</html>
