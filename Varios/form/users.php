<?php

require_once 'includes/init.php';

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="./js/main.js"></script>
    <title>Ejemplo Registro</title>
</head>
<body>
    <h1>CRUD usuarios</h1>

    <?php 
        $users = User::getUsers();

        $data = ["name" => 'Pepe', "pass"=>"1234", "id"=>"1"];
        $user = new User($data);
        echo $user-> updateUser();

    ?>
    <table>
        <thead>
            <th>
                <th>Id</th>
                <th>Nombre:</th>
                <th>Password:</th>
                <th>Actions:</th>
            </th>
        </thead>
        <tbody>
            <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user->getId() ?></td>
                <td><?= $user->getName() ?></td>
                <td><?= $user->getPass() ?></td>
                <td><button id="actualizar" name="actualizar">Actualizar</button></td>
                <td><button id="eliminar" name="eliminar">Eliminar</button></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <form action="" method="POST">
        
    </form>
</body>
</html>