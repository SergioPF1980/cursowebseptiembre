<?php
			
	class User{
			 	// Propiedades protegidas
			 	//No accesibles desde fuera, sólo por padre o hijos
				protected $myName;
				protected $pass;

				// Constructor
				public function __construct($entrada=false)
				{
					if(is_array($entrada))
					{
						foreach($entrada as $clave=>$valor)
						{
							$this->$clave=$valor;
						}
					}
				}

				//Métodos

				//Los siguientes permiten un acceso controlado a las propiedades, 
				//no de forma directa como antes
				public function getName(){
					return $this->myName;
				}
				
				public function getPass(){
					return $this->pass;
				}
				
				public function addRecord(){
				
					// Verifica los campos
					if ($this->_verifyInput()) {
						$connection = Database::getConnection();
						// Prepara la información
						$query = "INSERT INTO users(name, pass)
						VALUES ('" . Database::prep($this->myName) . "',
								'" . Database::prep($this->pass) . "'
							)";
						// Ejecuta la sentencia MySql
						if ($connection->query($query)) {
							$return = array('', 'Users añadido correctamente');
							return $return;
						}
						else {
							// Manda mensaje de error y regresa a contactmaint
							$return = array('contactmaint', 'No se ha añadido el contacto. No se ha podido añadir a la BD.');
							return $return;
						}
					}
					else {
						// Manda mensaje de error y regresa a contactmaint
						$return = array('contactmaint', 'No se ha añadido el contacto. Falta información obligatoria.');
						return $return;
					}
				}
		
				// Devuelve falso si ha habido error
				protected function _verifyInput() {
				$error = false;
					if (!trim($this->myName)) {
						$error = true;
					}
					if (!trim($this->pass)) {
						$error = true;
					}
					return !$error;
				}


				// Devuelve contactos
				public static function getContacts(){
					$items= array();
					$connection = Database::getConnection();
					$query="select * from contacts";
					
					if ($resultado = $connection->query($query)) {
			  			// Iterar sobre el resultado
						while ($obj = $resultado->fetch_object('Contact')) {
							// Añade objeto al vector
							$items[]=$obj;
						}
					// Libera resultado
					$resultado->close();
					}
					else{
						return false;
					}
					return $items;
				}

			}
			
?>