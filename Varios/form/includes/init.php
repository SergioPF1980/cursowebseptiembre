<?php
    session_start();
    include_once 'includes/function.php';

    spl_autoload_register(function ($class) {
        // include 'classes/' .$class . 'contact.php';
        require_once 'includes/classes/' . strtolower($class) . '.php';
    });
?>